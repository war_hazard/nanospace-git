﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using SimpleJSON;

public class ScoreAPI : MonoBehaviour
{
    private string Ki = "R4Ha5Ia N4n0M1LkY2020";
    private string status, token, msg, nama, email, pass, point_id;
    // Start is called before the first frame update
    void Start()
    {
        EncryptionKeys();
        token = EncryptedPlayerPrefs.GetString("token");
        GetSubmit();
    }

    private void EncryptionKeys()
    {
        EncryptedPlayerPrefs.keys = new string[5];
        EncryptedPlayerPrefs.keys[0] = "56Yyudra";
        EncryptedPlayerPrefs.keys[1] = "WI9DufYr";
        EncryptedPlayerPrefs.keys[2] = "idA5rAF2";
        EncryptedPlayerPrefs.keys[3] = "tHak3epr";
        EncryptedPlayerPrefs.keys[4] = "jat5eDTs";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetSubmit()
    {
        if (EncryptedPlayerPrefs.HasKey("login"))
        {
            StartCoroutine(GetPraSubmit());
        }
    }

    private IEnumerator GetPraSubmit()
    {
        string url = RequestManager.HOST + "pra-submitpoint";

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("authorization", token);
        headers.Add("key", Ki);
        WWW www = new WWW(url, null, headers);
        yield return www;
        Debug.Log(www.text);

        var N = JSONNode.Parse("www = " + www.text);

        status = N["status"];

        if (status == "true")
        {
            point_id = N["data"]["mypoint_id"];
            Debug.Log("msg: " + point_id);
        }
        else if (status == "false")
        {
            msg = N["message_system"][0];
            Debug.Log("msg: " + msg);
        }
    }
}
