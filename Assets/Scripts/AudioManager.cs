﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioManager : MonoBehaviour
{
    public GameObject musicOn;
    public GameObject musicOff;
    private AudioSource Audio;
    // Start is called before the first frame update
    void Start()
    {
       //Audio = Audio.GetComponent<AudioSource>();
        GameObject objs = GameObject.FindGameObjectWithTag("music");
        Audio = objs.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void On()
    {
        musicOn.SetActive(true);
        musicOff.SetActive(false);
        Audio.mute = true;
    }
    public void Off()
    {
        musicOn.SetActive(false);
        musicOff.SetActive(true);
        Audio.mute = false;
    }
}
