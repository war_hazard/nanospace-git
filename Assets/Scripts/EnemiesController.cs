﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemiesController : MonoBehaviour
{
    public float enemyIntensity = 0.5f;
    public GameObject prefabEnemyCommon;
    public GameObject prefabEnemyBoss;
    public int waveCounter = 0;
    public int enemyCount = 0;
    public int spawnedEnemyCount = 0;
    public Canvas uiCanvas;
    public bool isBossAlive = false;

    public AudioSource sfxBossCome;

    private float enemyIntensityIncrementer = 0.1f;

    private const int ENEMY_COUNT_FOR_ONE_INTENSITY = 10;

    // Start is called before the first frame update
    void Start()
    {
        NextWave();
    }

    // Update is called once per frame
    void Update()
    {
        //uiCanvas.transform.Find("Enemy Count").GetComponent<Text>().text = "Enemy : " + enemyCount.ToString();
        if (enemyCount <= 0 && !isBossAlive)
        {
            NextWave();
        }

        if (spawnedEnemyCount <= 0)
        {
            for (int i = 0; i < Mathf.Clamp(enemyIntensity * 5f, 0, enemyCount); i++)
            {
                spawnedEnemyCount++;
                short spawnPoint = (short)Random.Range(1, 18);

                Instantiate(prefabEnemyCommon, this.transform.Find("SpawnPoints").Find($"SpawnPoint_{spawnPoint}").position, prefabEnemyCommon.transform.localRotation, this.transform);
            }
        }
    }

    void NextWave()
    {
        waveCounter++;
        enemyCount = 0;
        SpawnEnemy();
        enemyIntensity += enemyIntensityIncrementer;
        enemyIntensityIncrementer += 0.001f;
        //uiCanvas.transform.Find("Wave Count").GetComponent<Text>().text = "Wave " + waveCounter.ToString();
    }

    void SpawnEnemy()
    {
        for (int i = 0; i < enemyIntensity * ENEMY_COUNT_FOR_ONE_INTENSITY; i++)
        {
            enemyCount++;
        }

        if (waveCounter % 10 == 0)
        {
            sfxBossCome.Play();
            Debug.Log(sfxBossCome);
            GameObject bossObject = Instantiate(prefabEnemyBoss, this.transform.Find("SpawnPoints").Find($"SpawnPoint_BOSS").position, prefabEnemyCommon.transform.localRotation, this.transform);
            bossObject.GetComponent<HealthController>().health *= waveCounter / 10;
            bossObject.tag = "Boss";

            isBossAlive = true;
        }
    }
}
