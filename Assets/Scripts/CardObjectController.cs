﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardObjectController : MonoBehaviour
{
    public bool isSidekickShip;
    public bool isCollidingWithWeaponPlacement = false;
    public GameObject WeaponPlacementObject;   
    private const string WeaponPlacementTagName = "WeaponPlacement";
    //public BoxCollider bc;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {


        //if (Input.GetKeyDown(KeyCode.Mouse0))
        //{
        //    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //    RaycastHit hit;

        //    if (Physics.Raycast(ray, out hit))
        //    {
        //        bc = hit.collider as BoxCollider;
        //        if (bc != null)
        //        {
        //            Destroy(bc.gameObject);
        //            CardController.sidekickIsSelected = false;
        //        }
        //        else if (bc = null)
        //            return;
        //    }
        //}

    }

    void OnMouseDown()
    {
        if(gameObject.CompareTag("sidekick"))
        {
            Destroy(gameObject);
            CardController.sidekickIsSelected = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Trigger Enter something");
        if (isSidekickShip == false)
        {
            if (other.CompareTag(WeaponPlacementTagName) && !other.GetComponent<WeaponPlacementController>().isOccupied)
            {
                isCollidingWithWeaponPlacement = true;
                WeaponPlacementObject = other.gameObject;
            }
        }
        else
        {
            return;
        }


    }

    private void OnTriggerExit(Collider other)
    {
        //Debug.Log("Trigger Exit something");
        isCollidingWithWeaponPlacement = false;
    }
}
