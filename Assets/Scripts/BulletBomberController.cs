﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class BulletBomberController : MonoBehaviour
{
    public float speed = 1;

    public string[] harmObjectsWithTag = { "Enemy", "Boss" };

    public float distanceShipToImpact;

    public GameObject explosionPrefab;

    public GameObject responsibleWeapon;

    // Start is called before the first frame update
    void Start()
    {
        /*GameObject[] rootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();

        foreach(GameObject rootGameObject in rootGameObjects)
        {
            if (rootGameObject.CompareTag("Player"))
            {
                m_Player = rootGameObject;
            }
        }*/
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (Vector3.Distance(responsibleWeapon.transform.position, transform.position) >= distanceShipToImpact)
        {
            SpawnExplosion();

            Destroy(this.gameObject);
        }
        else
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + speed * Time.deltaTime);
        }
    }

    private void SpawnExplosion()
    {
        Instantiate(explosionPrefab, transform.position, explosionPrefab.transform.rotation);
    }

    /*public void OnTriggerEnter(Collider collision)
    {
        for (int i = 0; i < harmObjectsWithTag.Length; i++)
        {
            if (collision.gameObject.CompareTag(harmObjectsWithTag[i]))
            {
                HealthController healthController;
                if (healthController = collision.gameObject.GetComponent<HealthController>())
                {
                    healthController.TakeDamage(damage);
                }
                Destroy(this.gameObject);
            }
        }
    }*/

    private void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }
}
