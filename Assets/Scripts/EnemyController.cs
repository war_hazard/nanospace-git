﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float enemySpeed = 1f;

    private Vector3 minPosition = new Vector3(-3.3f, 0.0f, 3.28f);
    private Vector3 maxPosition = new Vector3(3.22f, 0.0f, 10.23f);
    private Vector3 direction = new Vector3(0, 0, -1.0f);

    private float startingSpeed;

    // Start is called before the first frame update
    void Start()
    {
        Random.InitState((int)System.DateTime.Now.Ticks);
        InvokeRepeating("RequestChangeDirection", 1.0f, 3.0f);
        startingSpeed = enemySpeed;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Debug.DrawRay(transform.position, direction * 3f, Color.green);

        transform.position += direction * enemySpeed * Time.deltaTime;

        if (transform.position.x < minPosition.x || transform.position.z < minPosition.z || transform.position.x > maxPosition.x || transform.position.z > maxPosition.z)
        {
            Vector3 estimatedNextPositionFromOutside = transform.position + direction * 5f;
            if (estimatedNextPositionFromOutside.x < minPosition.x || estimatedNextPositionFromOutside.z < minPosition.z || estimatedNextPositionFromOutside.x > maxPosition.x || estimatedNextPositionFromOutside.z > maxPosition.z)
            {
                direction = (new Vector3(Random.Range(minPosition.x, maxPosition.x), 0.0f, Random.Range(minPosition.z, maxPosition.z)) - transform.position).normalized;
            }
            return;
        }

        Vector3 estimatedNextPosition = transform.position + direction;
        if (estimatedNextPosition.x < minPosition.x || estimatedNextPosition.z < minPosition.z || estimatedNextPosition.x > maxPosition.x || estimatedNextPosition.z > maxPosition.z)
        {
            ChangeDirection();
        }
    }

    void RequestChangeDirection()
    {
        bool isChangeDirection = ((Random.Range(0, 1000000) % 2) == 1 ? true : false);

        if (isChangeDirection)
            ChangeDirection();
    }

    void ChangeDirection()
    {
        direction = (new Vector3(Random.Range(-1f, 1f), 0.0f, Random.Range(-1f, 1f))).normalized;
    }

    public void ChangeMovementSpeedForSeconds(float seconds, float speed, bool isParalyzing)
    {
        StartCoroutine(ParalyzeForSeconds(seconds, speed, isParalyzing));
    }

    private IEnumerator ParalyzeForSeconds(float seconds, float speed, bool isParalyzing)
    {
        WeaponController[] weaponControllers = GetComponentsInChildren<WeaponController>();
        List<float> firerates = new List<float>();

        enemySpeed = speed;
        if (isParalyzing)
        {
            foreach (WeaponController weaponController in weaponControllers)
            {
                firerates.Add(weaponController.fireRate);
                weaponController.ChangeFirerate(0);
            }
        }


        yield return new WaitForSeconds(seconds);


        enemySpeed = startingSpeed;
        if (isParalyzing)
        {
            for (int i = 0; i < weaponControllers.Length; i++)
            {
                weaponControllers[i].fireRate = firerates[i];
            }
        }
    }
}