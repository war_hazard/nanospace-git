﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class StoryManager : MonoBehaviour
{
    public GameObject sp1, sp2, sp3, sp4, sp5, sp6;
    public Text number, loadingtext;
    public GameObject skipon, skipoff;
    // Start is called before the first frame update
    void Start()
    {
        skipoff.SetActive(true);
        skipon.SetActive(false);
        StartCoroutine(loading());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Change1(){
        sp1.SetActive(false);
        sp2.SetActive(true);
        number.text = "2";
    }
    public void Change2()
    {
        sp2.SetActive(false);
        sp3.SetActive(true);
        number.text = "3";
    }
    public void Change3()
    {
        sp3.SetActive(false);
        sp4.SetActive(true);
        number.text = "4";
    }
    public void Change4()
    {
        sp4.SetActive(false);
        sp5.SetActive(true);
        number.text = "5";
    }
    public void Change5()
    {
        sp5.SetActive(false);
        sp6.SetActive(true);
        number.text = "6";
    }

    public IEnumerator loading() {
        yield return new WaitForSeconds(3f);
        skipon.SetActive(true);
        skipoff.SetActive(false);
        loadingtext.text = "Sukses...";
    }
}
