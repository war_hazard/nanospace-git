﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using SimpleJSON;
using TMPro;

public class LeaderboardAPI : MonoBehaviour
{
    private string Ki = "R4Ha5Ia N4n0M1LkY2020";
    private string status, token, msg, nama, email, pass;

    public string eventnumber ="test";
    // Start is called before the first frame update

    //Leaderbord Variable
    int count_leader;
    public List<string> name_player;
    public List<string> score_player;

    public Text[] name_player_txt, score_player_txt;
    public TextMeshProUGUI[] name_player1_txt, score_player1_txt;

    public Text username, highscore;
    void Start()
    {
        EncryptionKeys();
        token = EncryptedPlayerPrefs.GetString("token");
        GetLeaderboard();
        getProfileCek();
    }
    private void EncryptionKeys()
    {
        EncryptedPlayerPrefs.keys = new string[5];
        EncryptedPlayerPrefs.keys[0] = "56Yyudra";
        EncryptedPlayerPrefs.keys[1] = "WI9DufYr";
        EncryptedPlayerPrefs.keys[2] = "idA5rAF2";
        EncryptedPlayerPrefs.keys[3] = "tHak3epr";
        EncryptedPlayerPrefs.keys[4] = "jat5eDTs";
    }

    public void getProfileCek()
    {
        if (EncryptedPlayerPrefs.HasKey("login"))
        {
            StartCoroutine(GetProfile());
        }
    }

    private IEnumerator GetProfile()
    {
        string url = RequestManager.HOST + "profile";

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("authorization", token);
        headers.Add("key", Ki);
        WWW www = new WWW(url, null, headers);
        yield return www;
        Debug.Log(www.text);

        var N = JSONNode.Parse(www.text);

        status = N["status"];

        if (status == "true")
        {
            msg = N["message_system"];
            Debug.Log("msg: " + msg);

            username.text = N["data"]["my_profile"]["name"];
            highscore.text = N["data"]["my_profile"]["point"];
        }
        else if (status == "false")
        {
            msg = N["message_system"][0];
            Debug.Log("msg: " + msg);
        }
    }

    public void GetLeaderboard()
    {
        if (EncryptedPlayerPrefs.HasKey("login"))
        {
            StartCoroutine(Leaderboard());
        }
    }
    private IEnumerator Leaderboard()
    {
        string url = RequestManager.HOST + "leaderboard";

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("authorization", token);
        headers.Add("key", Ki);

        WWWForm form = new WWWForm();
        form.AddField("event", eventnumber);

        WWW www = new WWW(url, form.data, headers);

        StartCoroutine(WaitForRequestGetLeaderboard(www));

        yield return null;
    }

    private IEnumerator WaitForRequestGetLeaderboard(WWW www)
    {
        yield return www;

        // check for errors
        if (www.error == null)
        {
            Debug.Log("WWW Ok!: " + www.text);

            var N = JSONNode.Parse(www.text);

            status = N["status"];
            Debug.Log("status: " + status);

            if (status == "true")
            {
                msg = N["message"];
                Debug.Log("msg: " + msg);

                name_player.Clear();
                score_player.Clear();

                count_leader = N["data"]["leaderboard"].Count;
                Debug.Log("count = " + count_leader);

                for (int i = 0; i < count_leader; i++)
                {
                    name_player.Add(N["data"]["leaderboard"][i]["name"]);
                    score_player.Add(N["data"]["leaderboard"][i]["point"]);

                    string player = name_player[i];
                    string score = score_player[i];

                    Debug.Log("name = " + player); // <--- tinggal ubah name_player[i]/player jadiin text
                    name_player_txt[i].text = player;
                    score_player_txt[i].text = score;
                    Debug.Log("score = " + score); // <--- tinggal ubah score_player[i]/score jadiin text

                    //name_player_txt[i].text = "test";
                    //score_player_txt[i].text = score;

                }
            }
            else if (status == "false")
            {
                if (N["data"]["message"].Count == 0)
                {
                    msg = N["data"]["message"];
                }
                else msg = N["data"]["message"][0];

                Debug.Log("msg: " + msg);
            }
        }
        else
        {
            Debug.Log("WWW Error: " + www.error);
        }
    }
}
