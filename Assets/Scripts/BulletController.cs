﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float speed = 1;

    public string[] harmObjectsWithTag = { "Enemy", "Boss"};

    public float damage = 10;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z + speed * Time.deltaTime);
    }

    public void OnTriggerEnter(Collider collision)
    {
        for (int i = 0; i < harmObjectsWithTag.Length; i++)
        {
            if (collision.gameObject.CompareTag(harmObjectsWithTag[i]))
            {
                HealthController healthController;
                if (healthController = collision.gameObject.GetComponent<HealthController>())
                {
                    healthController.TakeDamage(damage);
                }
                Destroy(this.gameObject);
            }
        }
    }

    private void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }
}
