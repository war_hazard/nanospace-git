﻿/*==============================================================================
Copyright (c) 2019 PTC Inc. All Rights Reserved.

Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Protected under copyright and other laws.
==============================================================================*/

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Vuforia;


/// <summary>
/// A custom handler that implements the ITrackableEventHandler interface.
///
/// Changes made to this file could be overwritten when upgrading the Vuforia version.
/// When implementing custom event handler behavior, consider inheriting from this class instead.
/// </summary>
public class EventHandler : MonoBehaviour, ITrackableEventHandler
{
    public GameObject border1;
    public GameObject borderlogo1;
    public GameObject border2;
    public GameObject borderlogo2;
    public GameObject arText1;
    public GameObject arText2;
    [HideInInspector]
    private Vector3 startPosition;
    public int pernahTrack = 0;
    public int MarkerIndex;
    public UnityEngine.UI.Image CurrentImg;
    public UnityEngine.UI.Image ShinnyStatusImg;
    public Sprite ShinnyOn;
    public Sprite ShinnyOff;

    //public GameObject NotScannedShooter;
    //public GameObject NotScannedBomber;
    //public GameObject NotScannedBouncher;
    //public GameObject NotScannedShock;
    //public GameObject NotScannedMutate;
    //public GameObject NotScannedBubble;
    //public GameObject NotScannedBulky;
    //public GameObject NotScannedKard;
    //public GameObject NotScannedBobo;
    //public GameObject NotScannedJellions;
    //public GameObject NotScannedSiNano;
    //public GameObject NotScannedTrioSika;
    //public GameObject NotScannedBarricade;

    bool bolehScan = false;

    public CardManager crdMngr;
    public bool pernahDiTrack = false;
    public bool isSpecialCard;
    public bool isShinny;

    public CardController crd;
    //public GameObject blocker;

    //Timer 
    //public Text[] DisplayDaysRemaining;
    //int index;

    //public TimerDaysRemaining IndicatorTimerDaysRemaining;
    #region PROTECTED_MEMBER_VARIABLES

    protected TrackableBehaviour mTrackableBehaviour;
    protected TrackableBehaviour.Status m_PreviousStatus;
    protected TrackableBehaviour.Status m_NewStatus;

    #endregion // PROTECTED_MEMBER_VARIABLES

    #region UNITY_MONOBEHAVIOUR_METHODS

    protected virtual void Start()
    {

        startPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        pernahTrack = 0;
        //PlayerPrefs.SetInt("ShooterCardScanned", 1);
        //PlayerPrefs.SetInt("BomberCardScanned", 1);
        //PlayerPrefs.SetInt("BouncherCardScanned", 1);
        //PlayerPrefs.SetInt("ShockCardScanned", 1);
        //PlayerPrefs.SetInt("MutateCardScanned", 1);
        //PlayerPrefs.SetInt("BubbleCardScanned", 1);
        //PlayerPrefs.SetInt("BulkyCardScanned", 1);
        //PlayerPrefs.SetInt("KARDCardScanned", 1);
        //PlayerPrefs.SetInt("BoBoCardScanned", 1);
        //PlayerPrefs.SetInt("JellionsCardScanned", 1);
        //PlayerPrefs.SetInt("SiNanoCardScanned", 1);
        //PlayerPrefs.SetInt("TrioSikaCardScanned", 1);
        //PlayerPrefs.SetInt("BarricadeCardScanned", 1);

        //NotScannedShooter.SetActive(PlayerPrefs.GetInt("ShooterCardScanned", 0) == 0 ? true : false);
        //NotScannedBomber.SetActive(PlayerPrefs.GetInt("BomberCardScanned", 0) == 0 ? true : false);
        //NotScannedBouncher.SetActive(PlayerPrefs.GetInt("BouncherCardScanned", 0) == 0 ? true : false);
        //NotScannedShock.SetActive(PlayerPrefs.GetInt("ShockCardScanned", 0) == 0 ? true : false);
        //NotScannedMutate.SetActive(PlayerPrefs.GetInt("MutateCardScanned", 0) == 0 ? true : false);
        //NotScannedBubble.SetActive(PlayerPrefs.GetInt("BubbleCardScanned", 0) == 0 ? true : false);
        //NotScannedBulky.SetActive(PlayerPrefs.GetInt("BulkyCardScanned", 0) == 0 ? true : false);
        //NotScannedKard.SetActive(PlayerPrefs.GetInt("KARDCardScanned", 0) == 0 ? true : false);
        //NotScannedBobo.SetActive(PlayerPrefs.GetInt("BoBoCardScanned", 0) == 0 ? true : false);
        //NotScannedJellions.SetActive(PlayerPrefs.GetInt("JellionsCardScanned", 0) == 0 ? true : false);
        //NotScannedSiNano.SetActive(PlayerPrefs.GetInt("SiNanoCardScanned", 0) == 0 ? true : false);
        //NotScannedTrioSika.SetActive(PlayerPrefs.GetInt("TrioSikaCardScanned", 0) == 0 ? true : false);
        //NotScannedBarricade.SetActive(PlayerPrefs.GetInt("BarricadeCardScanned", 0) == 0 ? true : false);



        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
    }

    protected virtual void OnDestroy()
    {
        if (mTrackableBehaviour)
            mTrackableBehaviour.UnregisterTrackableEventHandler(this);
    }

    #endregion // UNITY_MONOBEHAVIOUR_METHODS

    #region PUBLIC_METHODS

    /// <summary>
    ///     Implementation of the ITrackableEventHandler function called when the
    ///     tracking state changes.
    /// </summary>
    public void OnTrackableStateChanged(
        TrackableBehaviour.Status previousStatus,
        TrackableBehaviour.Status newStatus)
    {
        m_PreviousStatus = previousStatus;
        m_NewStatus = newStatus;

        Debug.Log("Trackable " + mTrackableBehaviour.TrackableName +
                  " " + mTrackableBehaviour.CurrentStatus +
                  " -- " + mTrackableBehaviour.CurrentStatusInfo);

        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
                 newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            OnTrackingLost();
        }
        else
        {
            // For combo of previousStatus=UNKNOWN + newStatus=UNKNOWN|NOT_FOUND
            // Vuforia is starting, but tracking has not been lost or found yet
            // Call OnTrackingLost() to hide the augmentations
            OnTrackingLost();
        }
    }

    #endregion // PUBLIC_METHODS

    #region PROTECTED_METHODS

    protected virtual void OnTrackingFound()
    {
        Debug.Log($"{transform.GetChild(0).gameObject.name} Marker Found!");

        if (mTrackableBehaviour)
        {
            var rendererComponents = mTrackableBehaviour.GetComponentsInChildren<Renderer>(true);
            var colliderComponents = mTrackableBehaviour.GetComponentsInChildren<Collider>(true);
            var canvasComponents = mTrackableBehaviour.GetComponentsInChildren<Canvas>(true);

            // Enable rendering:
            foreach (var component in rendererComponents)
                component.enabled = true;

            // Enable colliders:
            foreach (var component in colliderComponents)
                component.enabled = true;

            // Enable canvas':
            foreach (var component in canvasComponents)
                component.enabled = true;
        }
        border1.SetActive(false);
        borderlogo1.SetActive(false);
        border2.SetActive(false);
        borderlogo2.SetActive(false);
        arText1.SetActive(false);
        arText2.SetActive(false);

         
       
        if (MarkerIndex == 0)
        {
            //PlayerPrefs.SetInt("ShooterCardScanned", 1);
            pernahDiTrack = PlayerPrefs.GetInt("ShooterCardScanned", 0) == 0 ? false : true;
            
        }

        if (MarkerIndex == 2)
        {
            //PlayerPrefs.SetInt("BomberCardScanned", 1);
            pernahDiTrack = PlayerPrefs.GetInt("BomberCardScanned", 0) == 0 ? false : true;
        }

        if (MarkerIndex == 4)
        {
            //PlayerPrefs.SetInt("BouncherCardScanned", 1);
            pernahDiTrack = PlayerPrefs.GetInt("BouncherCardScanned", 0) == 0 ? false : true;
        }

        if (MarkerIndex == 6)
        {
            //PlayerPrefs.SetInt("ShockCardScanned", 1);
            pernahDiTrack = PlayerPrefs.GetInt("ShockCardScanned", 0) == 0 ? false : true;
            
        }

        if (MarkerIndex == 7)
        {
            //PlayerPrefs.SetInt("MutateCardScanned", 1);
            pernahDiTrack = PlayerPrefs.GetInt("MutateCardScanned", 0) == 0 ? false : true;
        }

        if (MarkerIndex == 8)
        {
            //PlayerPrefs.SetInt("BubbleCardScanned", 1);
            pernahDiTrack = PlayerPrefs.GetInt("BubbleCardScanned", 0) == 0 ? false : true;
        }

        if (MarkerIndex == 9)
        {
            //PlayerPrefs.SetInt("BulkyCardScanned", 1);
            pernahDiTrack = PlayerPrefs.GetInt("BulkyCardScanned", 0) == 0 ? false : true;
        }

        if (MarkerIndex == 10)
        {
            //PlayerPrefs.SetInt("KARDCardScanned", 1);
            pernahDiTrack = PlayerPrefs.GetInt("KARDCardScanned", 0) == 0 ? false : true;
        }

        if (MarkerIndex == 11)
        {
            //PlayerPrefs.SetInt("BoBoCardScanned", 1);
            pernahDiTrack = PlayerPrefs.GetInt("BoBoCardScanned", 0) == 0 ? false : true;
        }

        if (MarkerIndex == 12)
        {
            //PlayerPrefs.SetInt("JellionsCardScanned", 1);
            pernahDiTrack = PlayerPrefs.GetInt("JellionsCardScanned", 0) == 0 ? false : true;
        }

        if (MarkerIndex == 13)
        {
            //PlayerPrefs.SetInt("SiNanoCardScanned", 1);
            pernahDiTrack = PlayerPrefs.GetInt("SiNanoCardScanned", 0) == 0 ? false : true;
        }

        if (MarkerIndex == 14)
        {
            //PlayerPrefs.SetInt("TrioSikaCardScanned", 1);
            pernahDiTrack = PlayerPrefs.GetInt("TrioSikaCardScanned", 0) == 0 ? false : true;
        }

        if (MarkerIndex == 15)
        {
            //PlayerPrefs.SetInt("BarricadeCardScanned", 1);
            pernahDiTrack = PlayerPrefs.GetInt("BarricadeCardScanned", 0) == 0 ? false : true;
        }



        if (pernahTrack == 0) // Jika Belum Pernah Track Sama Sekali 
        {
            PlayerPrefs.SetInt("pernahTrack" + MarkerIndex.ToString(), 1);
            pernahTrack = PlayerPrefs.GetInt("pernahTrack" + MarkerIndex.ToString(), 0); // angka 0 sebagai default           
            bolehScan = true;

            // KONDISI BOLEH SCAN
            if (bolehScan)
            {

                if (PlayerPrefs.GetInt("pernahTrack" + MarkerIndex.ToString()) == 1) //setiap 1 kali scan akan mendapatkan skor,setelah lebih dari 1 scan maka sudah tidak bisa menambah skor
                {
                    
                    crdMngr.ScanKartu();
                }
            }
        }

    }


    protected virtual void OnTrackingLost()
    {
        if (mTrackableBehaviour)
        {
            var rendererComponents = mTrackableBehaviour.GetComponentsInChildren<Renderer>(true);
            var colliderComponents = mTrackableBehaviour.GetComponentsInChildren<Collider>(true);
            var canvasComponents = mTrackableBehaviour.GetComponentsInChildren<Canvas>(true);

            transform.position = startPosition;

            // Disable rendering:
            foreach (var component in rendererComponents)
                component.enabled = false;

            // Disable colliders:
            foreach (var component in colliderComponents)
                component.enabled = false;

            // Disable canvas':
            foreach (var component in canvasComponents)
                component.enabled = false;
        }

        border1.SetActive(true);
        borderlogo1.SetActive(true);
        border2.SetActive(true);
        borderlogo2.SetActive(true);
        arText1.SetActive(true);
        arText2.SetActive(true);
    }


    void OnApplicationQuit()
    {
        //PlayerPrefs.DeleteKey("pernahTrack" + MarkerIndex.ToString());

        //Debug.Log("PlayerPrefs Deleted");
    }

    void Update()
    {
        pernahTrack = PlayerPrefs.GetInt("pernahTrack" + MarkerIndex.ToString(), 0); // apakah marker udah pernah ditrack apa belum

        if (pernahTrack == 1)
        {
            crd.isScanned = true;
        }

        if (pernahTrack == 0)
        {
            crd.isScanned = false;
        }
    }


            
    

    #endregion // PROTECTED_METHODS
}
