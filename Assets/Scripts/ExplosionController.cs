﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionController : MonoBehaviour
{
    public float radius;
    public float explodeSpeed;
    public float explosionDamage;

    public string[] harmObjectsWithTag = { "Enemy", "Boss"};

    // Start is called before the first frame update
    void Start()
    {
        transform.localScale = Vector3.zero;
    }

    // Update is called once per frame
    void Update()
    {
        float explosionRadius = transform.localScale.magnitude;

        if (explosionRadius <= radius)
            transform.localScale += new Vector3(explodeSpeed * Time.deltaTime, explodeSpeed * Time.deltaTime, explodeSpeed * Time.deltaTime);
        else
            Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        for (int i = 0; i < harmObjectsWithTag.Length; i++)
        {
            if (other.CompareTag(harmObjectsWithTag[i]))
            {
                HealthController otherHealthController = other.gameObject.GetComponent<HealthController>();
                if (otherHealthController != null)
                    otherHealthController.TakeDamage(explosionDamage);
            }
        }
    }
}
