﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float playerSpeed;

    private Animator animator;
    private Vector3 m_PlaneEulerRotation = new Vector3(0, 0, 0);
    private float m_MaxPlaneSway = 45;
    private bool m_isPlaneParalized = false;
    private float bounceSpeed = 2f;


    Ray ray;
    Vector3 desiredPosition;
    Vector3 bouncePosition;
    Vector3 startMousePosition = Vector3.zero;
    Vector3 startPlanePosition = Vector3.zero;

    void Start()
    {
        transform.localRotation = Quaternion.Euler(m_PlaneEulerRotation);
        desiredPosition = GetComponent<Rigidbody>().position;
        animator = GetComponent<Animator>();
        StartCoroutine(DisableAnimationAfter());
    }

    IEnumerator DisableAnimationAfter()
    {
        yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length);

        animator.enabled = false;
    }

    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            startMousePosition = Input.mousePosition;
            startPlanePosition = transform.position;
            Debug.Log("StartMousePosition : " + startMousePosition);
        }

        if (Input.GetKey(KeyCode.Mouse0))
        {
            Vector3 changedPosition = Camera.main.ScreenPointToRay(Input.mousePosition).GetPoint(16f) - Camera.main.ScreenPointToRay(startMousePosition).GetPoint(16f);

            desiredPosition = startPlanePosition + changedPosition;
            desiredPosition.y = GetComponent<Rigidbody>().position.y;

            //transform.position = Vector3.MoveTowards(transform.position, desiredPosition, Vector3.Distance(transform.position, desiredPosition) / 16f * playerSpeed);

            //transform.localRotation = Quaternion.Euler(Mathf.Clamp(-m_MaxPlaneSway * ((desiredPosition.x - transform.position.x)), -m_MaxPlaneSway, m_MaxPlaneSway) + m_PlaneEulerRotation.x, m_PlaneEulerRotation.y, m_PlaneEulerRotation.z);

            //transform.localRotation = Quaternion.Euler(Vector3.RotateTowards(m_PlaneEulerRotation, new Vector3(m_PlaneEulerRotation.x, m_PlaneEulerRotation.y, (transform.position.x < desiredPosition.x ? m_PlaneEulerRotation.z - m_MaxPlaneSway : m_PlaneEulerRotation.z + m_MaxPlaneSway)), Mathf.Abs(transform.position.x - desiredPosition.x) / 8f * Time.deltaTime, m_MaxPlaneSway));

            //transform.localRotation = Quaternion.RotateTowards(transform.localRotation, Quaternion.FromToRotation(Vector3.up, new Vector3((transform.position.x < desiredPosition.x ? m_MaxPlaneSway : -m_MaxPlaneSway) * (transform.position.x - desiredPosition.x) / 8f, 0, 0)), Mathf.Abs(transform.position.x - desiredPosition.x));
        }

        if (!m_isPlaneParalized)
        {
            GetComponent<Rigidbody>().MovePosition(Vector3.MoveTowards(transform.position, desiredPosition, Vector3.Distance(transform.position, desiredPosition) / 13f * playerSpeed));
            transform.localRotation = Quaternion.RotateTowards(transform.localRotation, Quaternion.Euler(m_PlaneEulerRotation.x, m_PlaneEulerRotation.y, m_PlaneEulerRotation.z + Mathf.Clamp((desiredPosition.x - transform.position.x) / 2f * -m_MaxPlaneSway, -m_MaxPlaneSway, m_MaxPlaneSway)), Mathf.Abs(desiredPosition.x - transform.position.x) + 5.0f);
        }
        else
        {
            GetComponent<Rigidbody>().MovePosition(Vector3.MoveTowards(transform.position, bouncePosition, Vector3.Distance(transform.position, bouncePosition) / 13f * playerSpeed * bounceSpeed));
            if (Vector3.Distance(transform.position, bouncePosition) <= 0.1f)
            {
                m_isPlaneParalized = false;
                desiredPosition = GetComponent<Rigidbody>().position;
            }
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Boss")
        {
            bouncePosition = transform.position + (transform.position - collision.transform.position).normalized * 2f;

            m_isPlaneParalized = true;
            GetComponent<HealthController>().TakeDamage(10.0f);

            //Invoke("UnparalizePlane", 1);
        }
    }

    private void UnparalizePlane()
    {
        m_isPlaneParalized = false;
    }
}