﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerupsController : MonoBehaviour
{
    public enum Effects
    {
        CalciumOrb,
        ExEnergy,
        NanoNanoBombard
    }

    public GameObject powerupPrefab;
    public GameObject explosionPrefab;
    public GameObject calciumOrbSprite;
    public GameObject exEnergySprite;
    public GameObject nanoBombardSprite;
    public float powerupSpawnRateInSeconds;
    public float exEnergyDuration;
    public float nanoNanoBombardDuration;
    public float bombardSpeed;
    public float eachExplosionDamage;
    public float chancePercentageOfCalciumOrb;
    public float chancePercentageOfExEnergy;
    public float chancePercentageOfNanoBombard;

    private Vector3 minPosition = new Vector3(-3.3f, 0.0f, 3.28f);
    private Vector3 maxPosition = new Vector3(3.22f, 0.0f, 10.23f);

    // Start is called before the first frame update
    void Start()
    {
        Random.InitState((int)System.DateTime.Now.Ticks);
        InvokeRepeating("SpawnPowerUp", powerupSpawnRateInSeconds, powerupSpawnRateInSeconds);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void SpawnPowerUp()
    {
        if (Random.Range(1, 100) <= chancePercentageOfCalciumOrb)
        {
            float xOffset = Random.Range(-3f, 3f);

            Vector3 spawnPoint = transform.Find("SpawnPoint").position + new Vector3(xOffset, 0, 0);
            GameObject spawnedPowerUpObject = Instantiate(powerupPrefab, spawnPoint, powerupPrefab.transform.rotation, this.transform);
            spawnedPowerUpObject.GetComponent<PowerupController>().effect = Effects.CalciumOrb;
            GameObject spawnedCalciumOrbSprite = Instantiate(calciumOrbSprite, spawnedPowerUpObject.transform);
        }
        if (Random.Range(1, 100) <= chancePercentageOfExEnergy)
        {
            float xOffset = Random.Range(-3f, 3f);

            Vector3 spawnPoint = transform.Find("SpawnPoint").position + new Vector3(xOffset, 0, 0);
            GameObject spawnedPowerUpObject = Instantiate(powerupPrefab, spawnPoint, powerupPrefab.transform.rotation, this.transform);
            spawnedPowerUpObject.GetComponent<PowerupController>().effect = Effects.ExEnergy;
            GameObject spawnedExEnergySprite = Instantiate(exEnergySprite, spawnedPowerUpObject.transform);
        }
        if (Random.Range(1, 100) <= chancePercentageOfNanoBombard)
        {
            float xOffset = Random.Range(-3f, 3f);

            Vector3 spawnPoint = transform.Find("SpawnPoint").position + new Vector3(xOffset, 0, 0);
            GameObject spawnedPowerUpObject = Instantiate(powerupPrefab, spawnPoint, powerupPrefab.transform.rotation, this.transform);
            spawnedPowerUpObject.GetComponent<PowerupController>().effect = Effects.NanoNanoBombard;
            GameObject spawnedNanoBombardSprite = Instantiate(nanoBombardSprite, spawnedPowerUpObject.transform);
        }
    }

    public void RegisterPowerup(Effects effect, GameObject gameObjectToApply)
    {
        switch (effect)
        {
            case Effects.CalciumOrb:
                HealthController objectHealthController = gameObjectToApply.GetComponent<HealthController>();
                objectHealthController.health = Mathf.Clamp(objectHealthController.health + objectHealthController.maxHealth * 3 / 10, 0, objectHealthController.maxHealth);
                break;

            case Effects.ExEnergy:
                WeaponController[] weaponControllers = gameObjectToApply.GetComponentsInChildren<WeaponController>();

                foreach (WeaponController weaponController in weaponControllers)
                {
                    float oldFireRate = weaponController.fireRate;
                    StartCoroutine(ChangeFireRateForSeconds(weaponController, oldFireRate, oldFireRate * 3, exEnergyDuration));
                }
                break;

            case Effects.NanoNanoBombard:
                StartCoroutine(ActivateNanoNanoBombard());
                break;
        }
    }

    private IEnumerator ChangeFireRateForSeconds(WeaponController weaponController, float oldFireRate, float newFireRate, float forSeconds)
    {
        weaponController.ChangeFirerate(newFireRate);

        yield return new WaitForSeconds(forSeconds);

        weaponController.ChangeFirerate(oldFireRate);
    }

    private IEnumerator ActivateNanoNanoBombard()
    {
        InvokeRepeating("SpawnExplosion", 1 / (bombardSpeed * 5), 1 / (bombardSpeed * 5));

        yield return new WaitForSeconds(nanoNanoBombardDuration);

        CancelInvoke("SpawnExplosion");
    }

    private void SpawnExplosion()
    {
        Instantiate(explosionPrefab, new Vector3(Random.Range(minPosition.x, maxPosition.x), 0, Random.Range(minPosition.z, maxPosition.z)), explosionPrefab.transform.rotation, this.transform).GetComponent<ExplosionController>().explosionDamage = eachExplosionDamage;
    }
}
