﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupController : MonoBehaviour
{

    public float speed;
    public string[] powerupObjectsWithTag = { "Player" };
    public PowerupsController.Effects effect;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        transform.position += new Vector3(0, 0, -1 * speed * Time.deltaTime);
    }

    private void OnBecameInvisible()
    {
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        for (int i = 0; i < powerupObjectsWithTag.Length; i++)
        {
            if (other.CompareTag(powerupObjectsWithTag[i]))
            {
                this.GetComponentInParent<PowerupsController>().RegisterPowerup(effect, other.gameObject);
                Destroy(this.gameObject);
            }
        }
    }


}