﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShipExplosionPrefab : MonoBehaviour
{
    // Start is called before the first frame update

    private AudioSource scriptController;
    void Start()
    {
        StartCoroutine(DestroyThisAfter(GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length));
        
        
    }
    void Awake()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator DestroyThisAfter (float seconds)
    {
        scriptController = GameObject.FindGameObjectWithTag("explode").GetComponent<AudioSource>();
        scriptController.Play();
        yield return new WaitForSeconds(seconds);
        Destroy(this.gameObject);
    }
}
