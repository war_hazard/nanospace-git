﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TipsManager : MonoBehaviour
{
    public GameObject tips1, tips2, tips3, tips4, tips5, tips6;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(TipsSatu());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public IEnumerator TipsSatu()
    {
        tips6.SetActive(false);
        tips1.SetActive(true);
        yield return new WaitForSeconds(5f);
        tips1.SetActive(false);
        tips2.SetActive(true);
        yield return new WaitForSeconds(5f);
        tips2.SetActive(false);
        tips3.SetActive(true);
        yield return new WaitForSeconds(5f);
        tips3.SetActive(false);
        tips4.SetActive(true);
        yield return new WaitForSeconds(5f);
        tips4.SetActive(false);
        tips5.SetActive(true);
        yield return new WaitForSeconds(5f);
        tips5.SetActive(false);
        tips6.SetActive(true);
        yield return new WaitForSeconds(5f);
        StartCoroutine(TipsSatu());
    }
}
