﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class HangarController : MonoBehaviour
{
    public const int MAX_WEAPON_SLOT = 9;
    public const int MAX_COLOR_SLOT = 16;
    public GameObject uICanvas;
    public GameObject player;
    public string weaponPlacementTagName;
    public GameObject[] weaponCardPrefabs;
    public Material[] colorCardMaterials;

    public enum Cards
    {
        MilkyShooter,
        MilkyBomber,
        MilkyBouncer,
        MilkyShock,
        MilkyMutate,
        MilkyBubble,
        MilkyBulky,
        MilkyBarricadeTool,
        MilkyNano,
        MilkyBoBo,
        MilkyJellions,
        MilkySika,
        MilkyKard
    }

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 1; i <= HangarController.MAX_WEAPON_SLOT; i++)
        {
            if (PlayerPrefs.HasKey($"WeaponSlot{i}"))
            {
                GameObject weaponPlacement = player.transform.Find("WeaponPlacements").Find($"WeaponPlacement_{i}").gameObject;

                foreach(GameObject weaponCardPrefab in weaponCardPrefabs)
                {
                    if (weaponCardPrefab.name.Contains(PlayerPrefs.GetString($"WeaponSlot{i}")))
                    {
                        float x = PlayerPrefs.GetFloat($"WeaponSlot{i}_Position_X");
                        float y = PlayerPrefs.GetFloat($"WeaponSlot{i}_Position_Y");
                        float z = PlayerPrefs.GetFloat($"WeaponSlot{i}_Position_Z");
                        Instantiate(weaponCardPrefab, new Vector3(x, y, z), weaponCardPrefab.transform.rotation, weaponPlacement.transform).transform.localScale = Vector3.one;
                    }
                }
                
            }
        }

        for (int i = 1; i<= HangarController.MAX_COLOR_SLOT; i++)
        {
            if (PlayerPrefs.HasKey($"ColorSlot{i}"))
            {
                GameObject playerMesh = player.transform.Find($"PlayerMesh_{i}").gameObject;

                foreach (Material colorCardMaterial in colorCardMaterials)
                {
                    if (colorCardMaterial.name == PlayerPrefs.GetString($"ColorSlot{i}"))
                    {
                        playerMesh.GetComponent<MeshRenderer>().material = colorCardMaterial;
                    }
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0) && uICanvas.transform.Find("PanelHangar").gameObject.activeInHierarchy)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.gameObject.CompareTag(weaponPlacementTagName))
                {
                    int slot = hit.transform.GetComponent<WeaponPlacementController>().WeaponSlot;
                    hit.transform.GetComponent<WeaponPlacementController>().isOccupied = false;

                    RemoveWeapon(slot);
                    Destroy(hit.transform.GetChild(0).gameObject);
                }
            }
        }

        Scene currentScene = SceneManager.GetActiveScene();
        // Retrieve the name of this scene.
        string sceneName = currentScene.name;

        if (sceneName == "MainMenu")
        {
            player.SetActive(true);
        }

    }

    public static void RegisterWeapon(Cards card, int slot, Vector3 position)
    {
        Debug.Log("RegisterWeapon : Argument Card = " + card.ToString());
        Debug.Log("RegisterWeapon : Argument Slot = " + slot.ToString());
        Debug.Log("RegisterWeapon : Argument Position = " + position);
        PlayerPrefs.SetString($"WeaponSlot{slot}", System.Enum.GetName(typeof(Cards), card));
        PlayerPrefs.SetFloat($"WeaponSlot{slot}_Position_X", position.x);
        PlayerPrefs.SetFloat($"WeaponSlot{slot}_Position_Y", position.y);
        PlayerPrefs.SetFloat($"WeaponSlot{slot}_Position_Z", position.z);
    }

    public static void RemoveWeapon(int slot)
    {
        PlayerPrefs.DeleteKey($"WeaponSlot{slot}");
        PlayerPrefs.DeleteKey($"WeaponSlot{slot}_Position_X");
        PlayerPrefs.DeleteKey($"WeaponSlot{slot}_Position_Y");
        PlayerPrefs.DeleteKey($"WeaponSlot{slot}_Position_Z");
    }

    public static void RegisterColor(string meshSlot, string materialKey)
    {
        Debug.Log($"RegisterColor : Argument MeshSlot = {meshSlot}");
        Debug.Log($"RegisterColor : Argument MaterialKey = {materialKey}");

        PlayerPrefs.SetString($"ColorSlot{meshSlot}", materialKey);
    }

    public void ResetWeapon()
    {
        Debug.Log("all weapons reset");

        PlayerPrefs.DeleteKey($"WeaponSlot{1}");
        PlayerPrefs.DeleteKey($"WeaponSlot{1}_Position_X");
        PlayerPrefs.DeleteKey($"WeaponSlot{1}_Position_Y");
        PlayerPrefs.DeleteKey($"WeaponSlot{1}_Position_Z");

        PlayerPrefs.DeleteKey($"WeaponSlot{2}");
        PlayerPrefs.DeleteKey($"WeaponSlot{2}_Position_X");
        PlayerPrefs.DeleteKey($"WeaponSlot{2}_Position_Y");
        PlayerPrefs.DeleteKey($"WeaponSlot{2}_Position_Z");

        PlayerPrefs.DeleteKey($"WeaponSlot{3}");
        PlayerPrefs.DeleteKey($"WeaponSlot{3}_Position_X");
        PlayerPrefs.DeleteKey($"WeaponSlot{3}_Position_Y");
        PlayerPrefs.DeleteKey($"WeaponSlot{3}_Position_Z");

        PlayerPrefs.DeleteKey($"WeaponSlot{4}");
        PlayerPrefs.DeleteKey($"WeaponSlot{4}_Position_X");
        PlayerPrefs.DeleteKey($"WeaponSlot{4}_Position_Y");
        PlayerPrefs.DeleteKey($"WeaponSlot{4}_Position_Z");

        PlayerPrefs.DeleteKey($"WeaponSlot{5}");
        PlayerPrefs.DeleteKey($"WeaponSlot{5}_Position_X");
        PlayerPrefs.DeleteKey($"WeaponSlot{5}_Position_Y");
        PlayerPrefs.DeleteKey($"WeaponSlot{5}_Position_Z");

        PlayerPrefs.DeleteKey($"WeaponSlot{6}");
        PlayerPrefs.DeleteKey($"WeaponSlot{6}_Position_X");
        PlayerPrefs.DeleteKey($"WeaponSlot{6}_Position_Y");
        PlayerPrefs.DeleteKey($"WeaponSlot{6}_Position_Z");

        PlayerPrefs.DeleteKey($"WeaponSlot{7}");
        PlayerPrefs.DeleteKey($"WeaponSlot{7}_Position_X");
        PlayerPrefs.DeleteKey($"WeaponSlot{7}_Position_Y");
        PlayerPrefs.DeleteKey($"WeaponSlot{7}_Position_Z");

        PlayerPrefs.DeleteKey($"WeaponSlot{8}");
        PlayerPrefs.DeleteKey($"WeaponSlot{8}_Position_X");
        PlayerPrefs.DeleteKey($"WeaponSlot{8}_Position_Y");
        PlayerPrefs.DeleteKey($"WeaponSlot{8}_Position_Z");

        PlayerPrefs.DeleteKey($"WeaponSlot{9}");
        PlayerPrefs.DeleteKey($"WeaponSlot{9}_Position_X");
        PlayerPrefs.DeleteKey($"WeaponSlot{9}_Position_Y");
        PlayerPrefs.DeleteKey($"WeaponSlot{9}_Position_Z");

        PlayerPrefs.DeleteKey($"WeaponSlot{10}");
        PlayerPrefs.DeleteKey($"WeaponSlot{10}_Position_X");
        PlayerPrefs.DeleteKey($"WeaponSlot{10}_Position_Y");
        PlayerPrefs.DeleteKey($"WeaponSlot{10}_Position_Z");

        PlayerPrefs.DeleteKey($"WeaponSlot{11}");
        PlayerPrefs.DeleteKey($"WeaponSlot{11}_Position_X");
        PlayerPrefs.DeleteKey($"WeaponSlot{11}_Position_Y");
        PlayerPrefs.DeleteKey($"WeaponSlot{11}_Position_Z");

    }

    private void OnApplicationQuit()
    {
        GameController.sideKickSikaRight = false;
        GameController.sideKickBoBoRight = false;
        GameController.sideKickJellionsRight = false;
        GameController.sideKickSikaLeft = false;
        GameController.sideKickBoBoLeft = false;
        GameController.sideKickJellionsLeft = false;
    }
}
