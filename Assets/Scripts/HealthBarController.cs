﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarController : MonoBehaviour
{
    public GameObject objectControllingThisHealthBar;

    private float maxHealth;
    private float healthBarMaximumWidth;
    private HealthController m_ObjectHealthController;
    private RectTransform m_RectTransformBarOfHealthBar;

    // Start is called before the first frame update
    void Start()
    {
        m_ObjectHealthController = objectControllingThisHealthBar.GetComponent<HealthController>();
        maxHealth = m_ObjectHealthController.health;
        healthBarMaximumWidth = this.gameObject.GetComponent<RectTransform>().sizeDelta.x;
        m_RectTransformBarOfHealthBar = this.transform.Find("Bar").GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {
        m_RectTransformBarOfHealthBar.sizeDelta = new Vector2(healthBarMaximumWidth * (m_ObjectHealthController.health / maxHealth), m_RectTransformBarOfHealthBar.sizeDelta.y);
    }
}
