﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthController : MonoBehaviour
{
    public float health;

    private GameObject gameManager;
    public GameObject shipExplosionPrefab;
    public AudioSource sfxHitShip;

    [System.NonSerialized]
    public float maxHealth;

    private bool m_IsDestroyed = false;

    private void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        maxHealth = health;
        foreach (GameObject rootGameObject in UnityEngine.SceneManagement.SceneManager.GetActiveScene().GetRootGameObjects())
        {
            if (rootGameObject.name == "GameManager")
            {
                gameManager = rootGameObject;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void TakeDamage(float damage)
    {

        if (!m_IsDestroyed)
        {
            health -= damage;


            if (health <= 0)
            {
                m_IsDestroyed = true;

                if (gameObject.CompareTag("Enemy"))
                {
                    transform.parent.GetComponent<EnemiesController>().enemyCount--;
                    transform.parent.GetComponent<EnemiesController>().spawnedEnemyCount--;
                    gameManager.GetComponent<GameController>().killScore += 10000;
                    Destroy(this.gameObject);
                    Instantiate(shipExplosionPrefab, this.transform.position, shipExplosionPrefab.transform.rotation);

                }
                else if (gameObject.CompareTag("Boss"))
                {
                    transform.parent.GetComponent<EnemiesController>().isBossAlive = false;
                    gameManager.GetComponent<GameController>().killScore += 150000;
                    Destroy(this.gameObject);
                }
                else if (gameObject.CompareTag("Player"))
                {
                    gameManager.GetComponent<GameController>().GameOver();
                }

            }
            if (sfxHitShip != null)
            {
                sfxHitShip.Play();
            }
            else
                return;
        }
    }
}
