﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBouncerController : MonoBehaviour
{
    public float speed;

    public float attackRange;

    public string[] harmObjectsWithTag = { "Enemy", "Boss" };

    public float damage = 10;

    private bool m_IsAttacking = true;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (transform.localScale.z >= attackRange)
            m_IsAttacking = false;

        if (!m_IsAttacking && transform.localScale.z <= 0)
            Destroy(this.gameObject);

        if (m_IsAttacking)
            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z + speed * 25 * Time.deltaTime);
        else if (!m_IsAttacking)
            transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y, Mathf.Clamp(transform.localScale.z - speed * 25 * Time.deltaTime, 0, attackRange));

    }

    public void OnTriggerEnter(Collider collision)
    {
        for (int i = 0; i < harmObjectsWithTag.Length; i++)
        {
            if (collision.gameObject.CompareTag(harmObjectsWithTag[i]))
            {
                HealthController healthController;
                if (healthController = collision.gameObject.GetComponent<HealthController>())
                {
                    healthController.TakeDamage(damage);
                }
                m_IsAttacking = false;
            }
        }
    }

}
