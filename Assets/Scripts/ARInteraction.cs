﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ARInteraction : MonoBehaviour
{
    public Texture[] textures;
    public int currentTexture;
    public GameObject thirdObject;
    public ParticleSystem effect;
    public AudioSource Poff;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnMouseDown()
    {
        effect.Play(true);
        Poff.Play();
        currentTexture++;
        currentTexture %= textures.Length;
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = textures[1];
        thirdObject.SetActive(true);
    }
}
