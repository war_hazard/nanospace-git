﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using SimpleJSON;
using UnityEngine.Analytics;

public class GameController : MonoBehaviour
{
    public GameObject player;
    //public GameObject sikaManager;
    public static bool gemOver = false;
    public GameObject boboRight;
    public GameObject sikaRight;
    public GameObject jellionsRight;

    public GameObject boboLeft;
    public GameObject sikaLeft;
    public GameObject jellionsLeft;

    public int score = 0;
    public bool isPaused = false;

    public static bool sideKickBoBoRight = false;
    public static bool sideKickBoBoLeft = false;
    public static bool sideKickJellionsRight = false;
    public static bool sideKickJellionsLeft = false;
    public static bool sideKickSikaRight = false;
    public static bool sideKickSikaLeft = false;

    public string chanceNumber;

    /*[System.Serializable]
    public struct Weapon
    {
        public string name;
        public GameObject gameObject;
    }

    public Weapon[] weapons;*/

    public List<GameObject> WeaponCardPrefabs;

    public List<Material> ColorCardMaterials;

    [System.NonSerialized]
    public int killScore = 0;

    //API PURPOSE DONT TOUCH
    private string Ki = "R4Ha5Ia N4n0M1LkY2020";
    private string status, token, msg, nama, email, pass, point_id;
    public Text scoreAPI, timeAPI, loadinggame;

    public GameObject panelLoading;

    public GameObject howtoplay;
    public GameObject image1;
    public GameObject image2;
    public GameObject image3;
    public GameObject image4;
    public GameObject image5;
    public GameObject RightOn;
    public GameObject LeftOn;
    public GameObject RightOff;
    public GameObject LeftOff;
    public GameObject highscoreimage;
    public Text howtonumber, username;
    private string highscorelama;
    public int number;

    public int scorebaru, scorelama;

    public GameObject chance1, chance2, chance3, chance4, chance5, chanceKosongPanel;
    public AudioSource sfxShipSpawn, sfxShipMachine, highscore, newhighscore;
    void Start()
    {
        EncryptionKeys();
        token = EncryptedPlayerPrefs.GetString("token");
        GetSubmit();

        for (int i = 1; i <= HangarController.MAX_WEAPON_SLOT; i++)
        {
            if (PlayerPrefs.HasKey($"WeaponSlot{i}"))
            {
                GameObject weaponObject;
                string weaponCardKey = PlayerPrefs.GetString($"WeaponSlot{i}");

                foreach (GameObject weaponCardPrefab in WeaponCardPrefabs)
                {
                    if (weaponCardPrefab.name == weaponCardKey)
                    {
                        weaponObject = weaponCardPrefab;

                        Vector3 weaponPosition = new Vector3(PlayerPrefs.GetFloat($"WeaponSlot{i}_Position_X"), PlayerPrefs.GetFloat($"WeaponSlot{i}_Position_Y"), PlayerPrefs.GetFloat($"WeaponSlot{i}_Position_Z"));

                        Instantiate(weaponObject, transform.position + weaponPosition, weaponObject.transform.rotation, player.transform).transform.localPosition = weaponPosition;

                        break;
                    }
                }
            }
        }

        for (int i = 0; i <= HangarController.MAX_COLOR_SLOT; i++)
        {
            if (PlayerPrefs.HasKey($"ColorSlot{i}"))
            {
                Material fetchedMaterial;

                foreach (Material material in ColorCardMaterials)
                {
                    if (material.name == PlayerPrefs.GetString($"ColorSlot{i}"))
                    {
                        fetchedMaterial = material;
                        player.transform.Find($"PlayerMesh_{i}").GetComponent<MeshRenderer>().material = fetchedMaterial;
                    }
                }
            }
        }
        getProfileCek();
        number = 1;
        Time.timeScale = 0;
    }
    public void howtoplayOK()
    {
        howtoplay.SetActive(false);
        Time.timeScale = 1;
        sfxShipSpawn.Play();
        sfxShipMachine.Play();
    }

    public void Right()
    {
        number = number + 1;

        if (number > 5)
        {
            number = 5;
        }

    }

    public void Left()
    {
        number = number - 1;

        if (number < 1)
        {
            number = 1;
        }

    }

    void Update()
    {
        score = (int)(Time.timeSinceLevelLoad * 0) + killScore;

        transform.Find("Canvas").Find("Score").GetComponent<Text>().text = $"{score.ToString()}";
        transform.Find("Canvas").Find("Timer").GetComponent<Text>().text = $"{(int)Time.timeSinceLevelLoad}";

        if (sideKickSikaRight)
        {
            sikaRight.SetActive(true);
            sideKickSikaLeft = false;
        }

        if (sideKickSikaLeft)
        {
            sikaLeft.SetActive(true);
            sideKickSikaRight = false;
        }

        if (sideKickSikaRight == false)
        {
            sikaRight.SetActive(false);
        }

        if (sideKickSikaLeft == false)
        {
            sikaLeft.SetActive(false);
        }

        if (sideKickJellionsRight)
        {
            jellionsRight.SetActive(true);
            sideKickJellionsLeft = false;
        }

        if (sideKickJellionsLeft)
        {
            jellionsLeft.SetActive(true);
            sideKickJellionsRight = false;
        }

        if (sideKickJellionsRight == false)
        {
            jellionsRight.SetActive(false);
        }

        if (sideKickJellionsLeft == false)
        {
            jellionsLeft.SetActive(false);
        }

        if (sideKickBoBoRight)
        {
            boboRight.SetActive(true);
            sideKickBoBoLeft = false;
        }

        if (sideKickBoBoLeft)
        {
            boboLeft.SetActive(true);
            sideKickBoBoRight = false;
        }

        if (sideKickBoBoRight == false)
        {
            boboRight.SetActive(false);
        }

        if (sideKickBoBoLeft == false)
        {
            boboLeft.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            isPaused = !isPaused;

            if (isPaused)
            {
                Time.timeScale = 0;
                transform.Find("Canvas").Find("PanelPause").gameObject.SetActive(true);
            }
            else
            {
                Time.timeScale = 1;
                transform.Find("Canvas").Find("PanelPause").gameObject.SetActive(false);
            }
        }

        if (number == 1)
        {
            image1.SetActive(true);
            image2.SetActive(false);
            image3.SetActive(false);
            image4.SetActive(false);
            image5.SetActive(false);
            howtonumber.text = "1/5";
            LeftOn.SetActive(false);
            LeftOff.SetActive(true);
            RightOn.SetActive(true);
            RightOff.SetActive(false);
        }
        if (number == 2)
        {
            image1.SetActive(false);
            image2.SetActive(true);
            image3.SetActive(false);
            image4.SetActive(false);
            image5.SetActive(false);
            howtonumber.text = "2/5";
            LeftOn.SetActive(true);
            LeftOff.SetActive(false);
            RightOn.SetActive(true);
            RightOff.SetActive(false);
        }
        if (number == 3)
        {
            image1.SetActive(false);
            image2.SetActive(false);
            image3.SetActive(true);
            image4.SetActive(false);
            image5.SetActive(false);
            howtonumber.text = "3/5";
            LeftOn.SetActive(true);
            LeftOff.SetActive(false);
            RightOn.SetActive(true);
            RightOff.SetActive(false);
        }
        if (number == 4)
        {
            image1.SetActive(false);
            image2.SetActive(false);
            image3.SetActive(false);
            image4.SetActive(true);
            image5.SetActive(false);
            howtonumber.text = "4/5";
            LeftOn.SetActive(true);
            LeftOff.SetActive(false);
            RightOn.SetActive(true);
            RightOff.SetActive(false);
        }
        if (number == 5)
        {
            image1.SetActive(false);
            image2.SetActive(false);
            image3.SetActive(false);
            image4.SetActive(false);
            image5.SetActive(true);
            howtonumber.text = "5/5";
            LeftOn.SetActive(true);
            LeftOff.SetActive(false);
            RightOn.SetActive(false);
            RightOff.SetActive(true);
        }

        if (chanceNumber == "5")
        {
            chance1.SetActive(true);
            chance2.SetActive(true);
            chance3.SetActive(true);
            chance4.SetActive(true);
            chance5.SetActive(true);
            chanceKosongPanel.SetActive(false);
        }

        if (chanceNumber == "4")
        {
            chance1.SetActive(true);
            chance2.SetActive(true);
            chance3.SetActive(true);
            chance4.SetActive(true);
            chance5.SetActive(false);
            chanceKosongPanel.SetActive(false);
        }

        if (chanceNumber == "3")
        {
            chance1.SetActive(true);
            chance2.SetActive(true);
            chance3.SetActive(true);
            chance4.SetActive(false);
            chance5.SetActive(false);
            chanceKosongPanel.SetActive(false);
        }

        if (chanceNumber == "2")
        {
            chance1.SetActive(true);
            chance2.SetActive(true);
            chance3.SetActive(false);
            chance4.SetActive(false);
            chance5.SetActive(false);
            chanceKosongPanel.SetActive(false);
        }

        if (chanceNumber == "1")
        {
            chance1.SetActive(true);
            chance2.SetActive(false);
            chance3.SetActive(false);
            chance4.SetActive(false);
            chance5.SetActive(false);
            chanceKosongPanel.SetActive(false);
        }

        if (chanceNumber == "0")
        {
            chance1.SetActive(false);
            chance2.SetActive(false);
            chance3.SetActive(false);
            chance4.SetActive(false);
            chance5.SetActive(false);
            chanceKosongPanel.SetActive(true);
        }
    }

    public void GameOver()
    {
        Time.timeScale = 0;

        GameObject panelGameOver = transform.Find("Canvas").Find("PanelGameOver").gameObject;
        panelGameOver.SetActive(true);
        panelGameOver.transform.Find("Background").Find("Score").GetComponent<Text>().text = score.ToString();
        panelGameOver.transform.Find("Background").Find("Time").GetComponent<Text>().text = ((int)Time.timeSinceLevelLoad).ToString();

        sideKickSikaRight = false;
        sideKickBoBoRight = false;
        sideKickJellionsRight = false;
        sideKickSikaLeft = false;
        sideKickBoBoLeft = false;
        sideKickJellionsLeft = false;
        //player.SetActive(false);

        CekSubmitScore();
        checkChance();
        cekScore();
        gemOver = true;
        //gameObject.GetComponent<HangarPlayerMaterial>();


    }

    public void RestartGame()
    {
        //if (chanceNumber == "0")
        //{
        //    Debug.Log("ChanceZero");
        //    StartCoroutine(ChanceEmpty());
        //}else{
        SceneManager.LoadScene("Gameplay");
        Time.timeScale = 0;
        //}

    }

    //public IEnumerator ChanceEmpty()
    //{
    //    chanceKosongPanel.SetActive(true);
    //    yield return new WaitForSeconds(3f);
    //    chanceKosongPanel.SetActive(false);
    //}
    public void BackToMainMenu()
    {
        Time.timeScale = 1;
        StartCoroutine(BacktoMainMenu());
    }

    private IEnumerator BacktoMainMenu()
    {
        //GameObject panelLoading = uICanvas.transform.Find("PanelLoading").gameObject;

        panelLoading.SetActive(true);

        loadinggame.text = "...mohon menunggu...";
        //m_AsyncOperation = SceneManager.LoadSceneAsync(sceneName);
        yield return new WaitForSeconds(3f);
        Debug.Log("Scene Change");
        SceneManager.LoadScene("MainMenu");
        //m_AsyncOperation.allowSceneActivation = true;

        //while (!m_AsyncOperation.isDone)
        //{
        //    float progress = m_AsyncOperation.progress / 0.9f;
        //    panelLoading.transform.Find("Slider").GetComponent<Slider>().value = progress;
        //    loadinggame.text = "Tap Layar...";

        //    if (progress >= 1.0f && Input.GetKey(KeyCode.Mouse0))
        //    {
        //        m_AsyncOperation.allowSceneActivation = true;
        //    }

        //    yield return null;
        //}
    }

    //API ONLY DONT TOUCH

    private void EncryptionKeys()
    {
        EncryptedPlayerPrefs.keys = new string[5];
        EncryptedPlayerPrefs.keys[0] = "56Yyudra";
        EncryptedPlayerPrefs.keys[1] = "WI9DufYr";
        EncryptedPlayerPrefs.keys[2] = "idA5rAF2";
        EncryptedPlayerPrefs.keys[3] = "tHak3epr";
        EncryptedPlayerPrefs.keys[4] = "jat5eDTs";
    }

    public void GetSubmit()
    {
        if (EncryptedPlayerPrefs.HasKey("login"))
        {
            StartCoroutine(GetPraSubmit());
        }
    }

    public void CekSubmitScore()
    {
        if (EncryptedPlayerPrefs.HasKey("login"))
        {
            StartCoroutine(SubmitScore());
        }
    }

    public void cekScore()
    {
        scorebaru = Convert.ToInt32(scoreAPI);
        scorelama = Convert.ToInt32(highscorelama);
    }

    private IEnumerator GetPraSubmit()
    {
        string url = RequestManager.HOST + "pra-submitpoint";

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("authorization", token);
        headers.Add("key", Ki);
        WWW www = new WWW(url, null, headers);
        yield return www;
        Debug.Log(www.text);

        var N = JSONNode.Parse("www = " + www.text);

        status = N["status"];

        if (status == "true")
        {
            point_id = N["data"]["mypoint_id"];
            Debug.Log("msg: " + point_id);
        }
        else if (status == "false")
        {
            msg = N["message_system"][0];
            Debug.Log("msg: " + msg);
        }
    }

    private IEnumerator SubmitScore()
    {
        string url = RequestManager.HOST + "submit-point";

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("authorization", token);
        headers.Add("key", Ki);

        WWWForm form = new WWWForm();
        form.AddField("mypoint_id", point_id);
        form.AddField("score1", scoreAPI.text + 54321);
        form.AddField("score2", scoreAPI.text + 12345);
        form.AddField("timer", timeAPI.text);
        WWW www = new WWW(url, form.data, headers);

        StartCoroutine(WaitForRequestSubmitScore(www));

        yield return null;
    }

    private IEnumerator WaitForRequestSubmitScore(WWW www)
    {
        yield return www;

        // check for errors
        if (www.error == null)
        {
            Debug.Log("WWW Ok!: " + www.text);

            var N = JSONNode.Parse(www.text);

            status = N["status"];
            Debug.Log("status: " + status);

            if (status == "true")
            {
                msg = N["message_system"];
                Debug.Log("msg: " + msg);

                if (scorebaru < scorelama)
                {
                    highscoreimage.SetActive(false);
                    highscore.Play();
                }
                else
                {
                    highscoreimage.SetActive(true);
                    newhighscore.Play();
                }
            }
            else if (status == "false")
            {
                if (N["data"]["message_system"].Count == 0)
                {
                    msg = N["data"]["message_system"];
                }
                else msg = N["data"]["message_system"][0];

                Debug.Log("msg: " + msg);
            }
        }
        else
        {
            Debug.Log("WWW Error: " + www.error);
        }
    }

    public void checkChance()
    {
        if (EncryptedPlayerPrefs.HasKey("login"))
        {
            StartCoroutine(ChancePlay());
        }
    }

    private IEnumerator ChancePlay()
    {
        string url = RequestManager.HOST + "chance-play";

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("authorization", token);
        headers.Add("key", Ki);
        WWW www = new WWW(url, null, headers);
        yield return www;
        Debug.Log(www.text);

        var N = JSONNode.Parse(www.text);

        status = N["status"];

        if (status == "true")
        {
            msg = N["message_system"];
            Debug.Log("msg: " + msg);
            chanceNumber = N["data"]["my_chance"]["chance"];
        }
        else if (status == "false")
        {
            msg = N["message_system"][0];
            Debug.Log("msg: " + msg);
        }
    }


    public void getProfileCek()
    {
        if (EncryptedPlayerPrefs.HasKey("login"))
        {
            StartCoroutine(GetProfile());
        }
    }

    private IEnumerator GetProfile()
    {
        string url = RequestManager.HOST + "profile";

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("authorization", token);
        headers.Add("key", Ki);
        WWW www = new WWW(url, null, headers);
        yield return www;
        Debug.Log(www.text);

        var N = JSONNode.Parse(www.text);

        status = N["status"];

        if (status == "true")
        {
            msg = N["message_system"];
            Debug.Log("msg: " + msg);

            username.text = N["data"]["my_profile"]["name"];
            highscorelama = N["data"]["my_profile"]["point"];
        }
        else if (status == "false")
        {
            msg = N["message_system"][0];
            Debug.Log("msg: " + msg);
        }
    }

}
