﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;

public class ColorController : MonoBehaviour, IPointerDownHandler, IDragHandler, IEndDragHandler
{
    public Material MaterialToBeAppliedToShip;
    public static Material shipMaterial;
    public float SecondsToHoldBeforeDragging;
    public string[] PlayerMeshToApplyTag;
    public MeshRenderer LeftWing;
    public MeshRenderer RightWing;
    public MeshRenderer mainbody;
    public MeshRenderer bridge;
    public MeshRenderer mochiL;
    public MeshRenderer mochiR;
    public MeshRenderer thruster1;
    public MeshRenderer thruster2;
    public MeshRenderer thruster3;
    //public MeshRenderer Body;
    //public MeshRenderer LeftWing;
    //public MeshRenderer RightWing;


    private bool HadAppliedMaterial = false;
    private bool isDragging = false;
    private ScrollRect scrollRect;
    private Material previousMaterial = null;
    private GameObject previousGameObject = null;
    public GameObject gameObjectToApply;
    int index;
    private Vector3 startPosition;

    // Start is called before the first frame update
    void Start()
    {
        scrollRect = transform.parent.parent.parent.GetComponent<ScrollRect>();

        Vector3 anchoredPosition = GetComponent<RectTransform>().anchoredPosition;
        startPosition = new Vector3(anchoredPosition.x, anchoredPosition.y, anchoredPosition.z);
    }

    // Update is called once per frame
    void Update()
    {
        shipMaterial = previousMaterial;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        scrollRect.OnBeginDrag(eventData);
        Invoke("CountdownDrag", SecondsToHoldBeforeDragging);
    }

    private void CountdownDrag()
    {
        isDragging = true;
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (isDragging)
        {
            transform.position = Input.mousePosition;

            Ray ray = Camera.main.ScreenPointToRay(eventData.position);
            RaycastHit hit;
            gameObjectToApply = null;
            
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.gameObject.CompareTag(PlayerMeshToApplyTag[0]) || hit.transform.CompareTag(PlayerMeshToApplyTag[1])) // playership dan cardobject bertabrakan
                {
                    gameObjectToApply = hit.transform.gameObject;
                    //Material mat = Bodylist[index].material;
                    //Bodylist = gameObjectToApply.GetComponentsInChildren<MeshRenderer>();

                    //previousMaterial = mat;
                   
                    if (previousGameObject == null)
                    {
                        previousMaterial = gameObjectToApply.GetComponentInChildren<MeshRenderer>().material; // ngambil children dari kapal
                        previousGameObject = gameObjectToApply;
                        HadAppliedMaterial = true;                       
                    }

                    LeftWing.material = MaterialToBeAppliedToShip;
                    RightWing.material = MaterialToBeAppliedToShip;
                    mainbody.material = MaterialToBeAppliedToShip;
                    bridge.material = MaterialToBeAppliedToShip;
                    mochiL.material = MaterialToBeAppliedToShip;
                    mochiR.material = MaterialToBeAppliedToShip;
                    thruster1.material = MaterialToBeAppliedToShip;
                    thruster2.material = MaterialToBeAppliedToShip;
                    thruster3.material = MaterialToBeAppliedToShip;                
                }

                //if (hit.transform.gameObject.CompareTag(PlayerMeshToApplyTag[0]) || hit.transform.CompareTag(PlayerMeshToApplyTag[1]))
                //{
                //    gameObjectToApply = hit.transform.gameObject;
                //    //Material mat = Bodylist[index].material;
                //    //Bodylist = gameObjectToApply.GetComponentsInChildren<MeshRenderer>();

                //    //previousMaterial = mat;

                //    if (previousGameObject == null)
                //    {

                //        previousMaterial = gameObjectToApply.GetComponent<MeshRenderer>().material;
                //        previousGameObject = gameObjectToApply;
                //        HadAppliedMaterial = true;
                //        print("1");
                //    }


                //    //LeftWing.material = MaterialToBeAppliedToShip;
                //    //RightWing.material = MaterialToBeAppliedToShip;
                //    mainbody.material = MaterialToBeAppliedToShip;
                //    bridge.material = MaterialToBeAppliedToShip;
                //    //mochiL.material = MaterialToBeAppliedToShip;
                //    //mochiR.material = MaterialToBeAppliedToShip;
                //    //thruster1.material = MaterialToBeAppliedToShip;
                //    //thruster2.material = MaterialToBeAppliedToShip;
                //    //thruster3.material = MaterialToBeAppliedToShip;

                //    print("2");
                //}


            }
            else if (HadAppliedMaterial)
            {
                previousGameObject.GetComponent<MeshRenderer>().material = previousMaterial;
                print("3");
            }
        }
        else
        {
            CancelInvoke("CountdownDrag");
            scrollRect.OnDrag(eventData);
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (isDragging)
        {
            HangarController.RegisterColor(previousGameObject.name.Substring(previousGameObject.gameObject.name.Length - 1), MaterialToBeAppliedToShip.name);

            previousMaterial = null;
            previousGameObject = null;
            HadAppliedMaterial = false;

            isDragging = false;
            print("4");
        }
        else
            scrollRect.OnEndDrag(eventData);

        GetComponent<RectTransform>().anchoredPosition = startPosition;
        print("5");
    }
}
