﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SikaController : MonoBehaviour
{
    public GameObject planeToBeFollowed;
    public Vector3 sikaShipOffset = new Vector3(0,0,0);
    public float sikaShipSpeed;
    public static bool isFollowing = false;

    // Start is called before the first frame update
    void Start()
    {
        Scene currentScene = SceneManager.GetActiveScene();

        // Retrieve the name of this scene.
        string sceneName = currentScene.name;

        if (sceneName == "Gameplay")
        {
            isFollowing = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isFollowing)
        {
            GetComponent<Rigidbody>().MovePosition(Vector3.MoveTowards(transform.position, planeToBeFollowed.transform.position + sikaShipOffset, Vector3.Distance(transform.position, planeToBeFollowed.transform.position + sikaShipOffset) / 16f * sikaShipSpeed));
        }



        //transform.LeanMove(planeToBeFollowed.transform.position + sikaShipOffset, sikaShipSpeed / 0.5f);
    }

}
