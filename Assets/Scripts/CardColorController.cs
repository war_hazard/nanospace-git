﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using Vuforia;

public class CardColorController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, IEndDragHandler
{
    public GameObject CardColorObjectPrefab;    
    public float SecondsToHoldBeforeDragging;
    //public Sprite cardDetailImage;
    //public string cardTitle;
    //public float powerLevel;
    //public float attackSpeedLevel;    
    GameObject instantiatedCardObject;
    Vector3 startPosition;
    ScrollRect scrollRect;
    public static bool isDragging = false;
    public bool isScrolling = false;
    private GameObject uIManager;
    private const float WeaponCardObjectDistanceFromCamera = 3f;
    private const float WeaponCardObjectScaleWhenDragged = 1.5f;
    
    // Start is called before the first frame update
    void Start()
    {
        startPosition = GetComponent<RectTransform>().anchoredPosition;
        scrollRect = transform.parent.parent.parent.GetComponent<ScrollRect>();
        SikaController.isFollowing = true;
        foreach (GameObject rootGameObject in SceneManager.GetActiveScene().GetRootGameObjects())
        {
            if (rootGameObject.name == "UIManager")
            {
                uIManager = rootGameObject;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {


    }

    public void OnPointerDown(PointerEventData eventData)
    {

            Debug.Log("OnPointerDown");
            Invoke("CountdownDrag", SecondsToHoldBeforeDragging);
            scrollRect.OnBeginDrag(eventData);
        
        
    }

    private void CountdownDrag()
    {
        instantiatedCardObject = Instantiate(CardColorObjectPrefab, new Vector3(100, 100, 100), CardColorObjectPrefab.transform.rotation);
        instantiatedCardObject.transform.localScale *= WeaponCardObjectScaleWhenDragged;
        isDragging = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
            //if (!isScrolling && !isDragging)
            //{
            //    uIManager.GetComponent<UIController>().OnClickCard(cardDetailImage, cardTitle, powerLevel, attackSpeedLevel);
            //}
            CancelInvoke("CountdownDrag");
            OnEndDrag(eventData);              
    }

    public void OnDrag(PointerEventData eventData)
    {
        Debug.Log("OnDrag");

            if (isDragging)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                transform.position = Input.mousePosition;
                instantiatedCardObject.transform.position = ray.GetPoint(WeaponCardObjectDistanceFromCamera);
                //SikaController.isFollowing = false;
            }
            else
            {
                CancelInvoke("CountdownDrag");
                isScrolling = true;
                scrollRect.OnDrag(eventData);
            }
        
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("OnEndDrag");


        
            if (isDragging)
            {
            ColorCardObjectController ColorcardObjectController = instantiatedCardObject.GetComponent<ColorCardObjectController>();

                GetComponent<RectTransform>().anchoredPosition = startPosition;
               
                //if (!ColorcardObjectController.hitMesh) // gameobject destroyed if its not contained cardObjectController script
                //{

                  Destroy(instantiatedCardObject);

                //}
                //else
                //{
                //    // collider hit


                //}

                 isDragging = false;

            }
            else if (isScrolling)
            {
            CancelInvoke("CountdownDrag");
            scrollRect.OnEndDrag(eventData);
            isScrolling = false;
            }

        
    }
}
