﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using UnityEngine.UI;

public class MainMenuAPI : MonoBehaviour
{
    private string Ki = "R4Ha5Ia N4n0M1LkY2020";
    private string status, token, msg, nama, email, pass;

    //private string nama_user, score;
    public Text username, highscore;
    public string chanceNumber;
    public GameObject chance1, chance2, chance3, chance4, chance5;
    // Start is called before the first frame update
    void Start()
    {
        EncryptionKeys();
        token = EncryptedPlayerPrefs.GetString("token");
        getProfileCek();
        checkChance();
    }
    private void EncryptionKeys()
    {
        EncryptedPlayerPrefs.keys = new string[5];
        EncryptedPlayerPrefs.keys[0] = "56Yyudra";
        EncryptedPlayerPrefs.keys[1] = "WI9DufYr";
        EncryptedPlayerPrefs.keys[2] = "idA5rAF2";
        EncryptedPlayerPrefs.keys[3] = "tHak3epr";
        EncryptedPlayerPrefs.keys[4] = "jat5eDTs";
    }
    public void getProfileCek()
    {
        if (EncryptedPlayerPrefs.HasKey("login"))
        {
            StartCoroutine(GetProfile());
        }
    }

    private IEnumerator GetProfile()
    {
        string url = RequestManager.HOST + "profile";

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("authorization", token);
        headers.Add("key", Ki);
        WWW www = new WWW(url, null, headers);
        yield return www;
        Debug.Log(www.text);

        var N = JSONNode.Parse(www.text);

        status = N["status"];

        if (status == "true")
        {
            msg = N["message_system"];
            Debug.Log("msg: " + msg);

            username.text = N["data"]["my_profile"]["name"];
            highscore.text = N["data"]["my_profile"]["point"];
        }
        else if (status == "false")
        {
            msg = N["message_system"][0];
            Debug.Log("msg: " + msg);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (chanceNumber == "5")
        {
            chance1.SetActive(true);
            chance2.SetActive(true);
            chance3.SetActive(true);
            chance4.SetActive(true);
            chance5.SetActive(true);
        }

        if (chanceNumber == "4")
        {
            chance1.SetActive(true);
            chance2.SetActive(true);
            chance3.SetActive(true);
            chance4.SetActive(true);
            chance5.SetActive(false);
        }

        if (chanceNumber == "3")
        {
            chance1.SetActive(true);
            chance2.SetActive(true);
            chance3.SetActive(true);
            chance4.SetActive(false);
            chance5.SetActive(false);
        }

        if (chanceNumber == "2")
        {
            chance1.SetActive(true);
            chance2.SetActive(true);
            chance3.SetActive(false);
            chance4.SetActive(false);
            chance5.SetActive(false);
        }

        if (chanceNumber == "1")
        {
            chance1.SetActive(true);
            chance2.SetActive(false);
            chance3.SetActive(false);
            chance4.SetActive(false);
            chance5.SetActive(false);
        }

        if (chanceNumber == "0")
        {
            chance1.SetActive(false);
            chance2.SetActive(false);
            chance3.SetActive(false);
            chance4.SetActive(false);
            chance5.SetActive(false);
        }
    }

    public void checkChance()
    {
        if (EncryptedPlayerPrefs.HasKey("login"))
        {
            StartCoroutine(ChancePlay());
        }
    }

    private IEnumerator ChancePlay()
    {
        string url = RequestManager.HOST + "chance-play";

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("authorization", token);
        headers.Add("key", Ki);
        WWW www = new WWW(url, null, headers);
        yield return www;
        Debug.Log(www.text);

        var N = JSONNode.Parse(www.text);

        status = N["status"];

        if (status == "true")
        {
            msg = N["message_system"];
            Debug.Log("msg: " + msg);
            chanceNumber = N["data"]["my_chance"]["chance"];
        }
        else if (status == "false")
        {
            msg = N["message_system"][0];
            Debug.Log("msg: " + msg);
        }
    }
}
