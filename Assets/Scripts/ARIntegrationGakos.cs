﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARIntegrationGakos : MonoBehaviour
{
    public GameObject Object1;
    public GameObject Object2;
    public GameObject Object3;
    public GameObject Object4;
    public GameObject Object5;
    public GameObject Object6;
    public GameObject Object7;
    public GameObject Object8;
    public GameObject Object9;
    public GameObject Object10;
    public AudioSource poff;

    public ParticleSystem effect;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnMouseDown()
    {
        effect.Play(true);
        Object1.SetActive(true);
        Object2.SetActive(true);
        Object3.SetActive(true);
        Object4.SetActive(true);
        Object5.SetActive(true);
        Object6.SetActive(true);
        Object7.SetActive(true);
        Object8.SetActive(true);
        Object9.SetActive(true);
        Object10.SetActive(true);
        poff.Play();
    }
}
