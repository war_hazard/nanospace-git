﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public GameObject bulletPrefab;
    public float fireRate;
    public float bulletSpeed;
    public float damage = 10;
    public string[] harmObjectsWithTag = { "Enemy", "Boss" };
    
    public AudioSource bulletdef;

    // Start is called before the first frame update
    void Start()
    {
        ChangeFirerate(this.fireRate);
    }

    // Update is called once per frame
    void Update()
    {
 

    }

    public void ChangeFirerate(float newFireRate)
    {

        float spawnBulletAfterSeconds = 1 / (newFireRate / 60);
        CancelInvoke("SpawnBullet");
        InvokeRepeating("SpawnBullet", spawnBulletAfterSeconds, spawnBulletAfterSeconds);

    }

    private void SpawnBullet()
    {
        if (bulletPrefab.gameObject.name == "BulletDefault" || bulletPrefab.gameObject.name == "BulletEnemy")
        {
            GameObject spawnedBullet = Instantiate(bulletPrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z), bulletPrefab.transform.rotation);
            //bulletdef.Play();
            spawnedBullet.GetComponent<BulletController>().harmObjectsWithTag = this.harmObjectsWithTag;
            spawnedBullet.GetComponent<BulletController>().speed = bulletSpeed;
            spawnedBullet.GetComponent<BulletController>().damage = this.damage;
        }
        else if (bulletPrefab.gameObject.name == "BulletBomber")
        {
            GameObject spawnedBullet = Instantiate(bulletPrefab, transform.position, bulletPrefab.transform.rotation);
            spawnedBullet.GetComponent<BulletBomberController>().harmObjectsWithTag = this.harmObjectsWithTag;
            spawnedBullet.GetComponent<BulletBomberController>().speed = bulletSpeed;
            spawnedBullet.GetComponent<BulletBomberController>().responsibleWeapon = this.gameObject;
        }
        else if (bulletPrefab.gameObject.name == "BulletBouncer")
        {
            GameObject spawnedMeleeBullet = Instantiate(bulletPrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z), bulletPrefab.transform.rotation);
            spawnedMeleeBullet.GetComponent<BulletBouncerController>().harmObjectsWithTag = this.harmObjectsWithTag;
            spawnedMeleeBullet.GetComponent<BulletBouncerController>().speed = bulletSpeed;
            spawnedMeleeBullet.GetComponent<BulletBouncerController>().damage = this.damage;
            spawnedMeleeBullet.transform.parent = this.gameObject.transform;
        }
        else if (bulletPrefab.gameObject.name == "BulletShock")
        {
            GameObject spawnedBullet = Instantiate(bulletPrefab, transform.position, bulletPrefab.transform.rotation);
            spawnedBullet.GetComponent<BulletBomberController>().harmObjectsWithTag = this.harmObjectsWithTag;
            spawnedBullet.GetComponent<BulletBomberController>().speed = bulletSpeed;
            spawnedBullet.GetComponent<BulletBomberController>().responsibleWeapon = this.gameObject;
        }
        else if (bulletPrefab.gameObject.name == "BulletSika")
        {
            GameObject spawnedBullet = Instantiate(bulletPrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z), bulletPrefab.transform.rotation);
            
            spawnedBullet.GetComponent<BulletController>().harmObjectsWithTag = this.harmObjectsWithTag;
            spawnedBullet.GetComponent<BulletController>().speed = bulletSpeed;
            spawnedBullet.GetComponent<BulletController>().damage = this.damage;
        }
        else if (bulletPrefab.gameObject.name == "BulletJellions")
        {
            GameObject spawnedBullet = Instantiate(bulletPrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z), bulletPrefab.transform.rotation);

            spawnedBullet.GetComponent<BulletController>().harmObjectsWithTag = this.harmObjectsWithTag;
            spawnedBullet.GetComponent<BulletController>().speed = bulletSpeed;
            spawnedBullet.GetComponent<BulletController>().damage = this.damage;
        }
        else if (bulletPrefab.gameObject.name == "BulletBoBo")
        {
            GameObject spawnedBullet = Instantiate(bulletPrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z), bulletPrefab.transform.rotation);

            spawnedBullet.GetComponent<BulletController>().harmObjectsWithTag = this.harmObjectsWithTag;
            spawnedBullet.GetComponent<BulletController>().speed = bulletSpeed;
            spawnedBullet.GetComponent<BulletController>().damage = this.damage;
        }
        else if (bulletPrefab.gameObject.name == "BulletBulky")
        {
            GameObject spawnedBullet = Instantiate(bulletPrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z), bulletPrefab.transform.rotation);

            spawnedBullet.GetComponent<BulletController>().harmObjectsWithTag = this.harmObjectsWithTag;
            spawnedBullet.GetComponent<BulletController>().speed = bulletSpeed;
            spawnedBullet.GetComponent<BulletController>().damage = this.damage;
        }
        else if (bulletPrefab.gameObject.name == "BulletShooter")
        {
            GameObject spawnedBullet = Instantiate(bulletPrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z), bulletPrefab.transform.rotation);

            spawnedBullet.GetComponent<BulletController>().harmObjectsWithTag = this.harmObjectsWithTag;
            spawnedBullet.GetComponent<BulletController>().speed = bulletSpeed;
            spawnedBullet.GetComponent<BulletController>().damage = this.damage;
        }
        else if (bulletPrefab.gameObject.name == "BulletNano")
        {
            GameObject spawnedBullet = Instantiate(bulletPrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z), bulletPrefab.transform.rotation);

            spawnedBullet.GetComponent<BulletController>().harmObjectsWithTag = this.harmObjectsWithTag;
            spawnedBullet.GetComponent<BulletController>().speed = bulletSpeed;
            spawnedBullet.GetComponent<BulletController>().damage = this.damage;
        }
    }
}
