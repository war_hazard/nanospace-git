﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using Vuforia;

public class CardController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler, IEndDragHandler
{
    public GameObject CardObjectPrefab;
    public HangarController.Cards Card;
    public float SecondsToHoldBeforeDragging;
    public Sprite cardDetailImage;
    public string cardTitle;
    public float powerLevel;
    public float attackSpeedLevel;

    
    GameObject instantiatedCardObject;
    Vector3 startPosition;
    ScrollRect scrollRect;
    public static bool isDragging = false;
    public bool isScrolling = false;
    public bool isScanned;
    public bool isSidekickShip;
    public static bool sidekickIsSelected = false;
    public GameObject Blocker;
    public GameObject[] sideKickUsedImg;
    private GameObject uIManager;
    private const float WeaponCardObjectDistanceFromCamera = 3f;
    private const float WeaponCardObjectScaleWhenDragged = 1.5f;
    
    // Start is called before the first frame update
    void Start()
    {
        startPosition = GetComponent<RectTransform>().anchoredPosition;
        scrollRect = transform.parent.parent.parent.GetComponent<ScrollRect>();
        SikaController.isFollowing = true;
        foreach (GameObject rootGameObject in SceneManager.GetActiveScene().GetRootGameObjects())
        {
            if (rootGameObject.name == "UIManager")
            {
                uIManager = rootGameObject;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(isScanned)
        {
            Blocker.SetActive(false);
        }

        if (isScanned == false)
        {
            Blocker.SetActive(true);
        }

        if (sidekickIsSelected == false)
        {

            if (sideKickUsedImg != null)
            {
                sideKickUsedImg[0].SetActive(false);
                sideKickUsedImg[1].SetActive(false);
                sideKickUsedImg[2].SetActive(false);
            }
            else
            {
                return;
            }
        }

    }

    public void OnPointerDown(PointerEventData eventData)
    {

            Debug.Log("OnPointerDown");
            Invoke("CountdownDrag", SecondsToHoldBeforeDragging);
            scrollRect.OnBeginDrag(eventData);
        
        
    }

    private void CountdownDrag()
    {
        instantiatedCardObject = Instantiate(CardObjectPrefab, new Vector3(100, 100, 100), CardObjectPrefab.transform.rotation);
        instantiatedCardObject.transform.localScale *= WeaponCardObjectScaleWhenDragged;
        isDragging = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if (isScanned)
        {
            if (!isScrolling && !isDragging)
            {
                uIManager.GetComponent<UIController>().OnClickCard(cardDetailImage, cardTitle, powerLevel, attackSpeedLevel);
            }
            CancelInvoke("CountdownDrag");
            OnEndDrag(eventData);
        }
        
    }

    public void OnDrag(PointerEventData eventData)
    {
        Debug.Log("OnDrag");
        if (isScanned)
        {
            if (isDragging)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                transform.position = Input.mousePosition;
                instantiatedCardObject.transform.position = ray.GetPoint(WeaponCardObjectDistanceFromCamera);
                SikaController.isFollowing = false;
            }
            else
            {
                CancelInvoke("CountdownDrag");
                isScrolling = true;
                scrollRect.OnDrag(eventData);
            }
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("OnEndDrag");
        if (isScanned)
        {

        
            if (isDragging)
            {
                CardObjectController cardObjectController = instantiatedCardObject.GetComponent<CardObjectController>();

                GetComponent<RectTransform>().anchoredPosition = startPosition;

                

                if (!cardObjectController.isCollidingWithWeaponPlacement) // gameobject destroyed if its not contained cardObjectController script
                {
                    if (isSidekickShip == false)
                    {
                        Destroy(instantiatedCardObject);
                    }

                    if (isSidekickShip)
                    {
                        if (sidekickIsSelected)
                        {
                            Destroy(instantiatedCardObject);
                        }
                    }
                }
                else
                {

                    // weapon placement

                    WeaponPlacementController weaponPlacementController = instantiatedCardObject.GetComponent<CardObjectController>().WeaponPlacementObject.GetComponent<WeaponPlacementController>();

                    instantiatedCardObject.transform.position = new Vector3(cardObjectController.WeaponPlacementObject.transform.position.x, cardObjectController.WeaponPlacementObject.transform.position.y, cardObjectController.WeaponPlacementObject.transform.position.z);
                    instantiatedCardObject.transform.localScale /= WeaponCardObjectScaleWhenDragged;
                    instantiatedCardObject.transform.parent = weaponPlacementController.transform;

                    weaponPlacementController.isOccupied = true;
                    HangarController.RegisterWeapon(Card, weaponPlacementController.WeaponSlot, instantiatedCardObject.transform.position - instantiatedCardObject.transform.parent.parent.parent.position);
                }

                 isDragging = false;

                 if(isSidekickShip)
                 {
                    SikaController.isFollowing = true;
                    sidekickIsSelected = true;
                    sideKickUsedImg[0].SetActive(true);
                    sideKickUsedImg[1].SetActive(true);
                    sideKickUsedImg[2].SetActive(true);
                }
            }
            else if (isScrolling)
            {
            CancelInvoke("CountdownDrag");
            scrollRect.OnEndDrag(eventData);
            isScrolling = false;
            }

        }
    }
}
