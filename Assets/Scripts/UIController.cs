﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class UIController : MonoBehaviour
{
    public GameObject uICanvas;
    public GameObject BuddyPlacementCanvas;
    public GameObject player;
    public GameObject aRSpaceCandyPopUp;
    public GameObject aRScanCard;
    public Camera arCam;
    public GameObject aRCamera;
    public GameObject hangarBackground;
    public GameObject effects;

    public GameObject border1;
    public GameObject borderlogo1;
    public GameObject border2;
    public GameObject borderlogo2;
    public GameObject arText1;
    public GameObject arText2;

    public GameObject[] CardTarget;

    public GameObject settingButton;
    public GameObject settingPanel;
    public GameObject settingClose;

    public GameObject chanceScript;
    public MainMenuAPI chanceObject;
    public GameObject chanceKosongPanel;

    private AsyncOperation m_AsyncOperation;
    private GameObject clonedPlayer;

    public AudioSource HangarSound;

    public Text loadinggame;
    public static bool BuddyDragged1 = false;
    public static bool BuddyDragged2 = false;
    //public GameObject uiBuddy1;
    //public GameObject uiBuddy2;
    public HangarController hangarcon;
    bool isReset = false;
    private string scanCardTag = "ScanCardTarget";
    private const string m_PlayerShipTagName = "PlayerShip";
    private const float MAX_POWER_LEVEL = 5.0f;

    private void Awake()
    {
        aRCamera.SetActive(true);
    }

    // Start is called before the first frame update
    void Start()
    {
        //aRCamera.SetActive(false);
        arCam.enabled = false;
        chanceObject = chanceScript.GetComponent<MainMenuAPI>();
        Vuforia.VuforiaBehaviour.Instance.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Mouse0) && !player.GetComponent<Animator>().GetBool("IsInHangar"))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.CompareTag(m_PlayerShipTagName))
                {
                    OnClickPlayerShip();

                    if (isReset == false)
                    {
                        hangarcon.ResetWeapon();
                        isReset = true;
                    }
                    if (isReset)
                    {
                        return;
                    }
                }
            }
        }

        //if(BuddyDragged1)
        //{
        //    uiBuddy1.SetActive(false);
        //}

        //if (BuddyDragged2)
        //{
        //    uiBuddy2.SetActive(false);
        //}

        //if (BuddyDragged1 == false)
        //{
        //    uiBuddy1.SetActive(true);
        //}

        //if (BuddyDragged2 == false)
        //{
        //    uiBuddy2.SetActive(true);
        //}
    }

    private IEnumerator LoadSceneAsynchronously(string sceneName)
    {
        GameObject panelLoading = uICanvas.transform.Find("PanelLoading").gameObject;

        panelLoading.SetActive(true);
        loadinggame.text = "...mohon menunggu...";
        //m_AsyncOperation = SceneManager.LoadSceneAsync(sceneName);
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene(sceneName);
        //m_AsyncOperation.allowSceneActivation = true;

        //while (!m_AsyncOperation.isDone)
        //{
        //    float progress = m_AsyncOperation.progress / 0.9f;
        //    panelLoading.transform.Find("Slider").GetComponent<Slider>().value = progress;
        //    loadinggame.text = "Tap Layar...";

        //    if (progress >= 1.0f && Input.GetKey(KeyCode.Mouse0))
        //    {
        //        m_AsyncOperation.allowSceneActivation = true;
        //    }

        //    yield return null;
        //}
    }

    public void OnClickInventoryWeapon()
    {
        GameObject buttonWeapon = uICanvas.transform.Find("PanelHangar").Find("PanelInventory").Find("PanelTab").Find("Weapon").gameObject;
        GameObject buttonColor = uICanvas.transform.Find("PanelHangar").Find("PanelInventory").Find("PanelTab").Find("Color").gameObject;
        GameObject panelWeapon = uICanvas.transform.Find("PanelHangar").Find("PanelInventory").Find("PanelWeapon").gameObject;
        GameObject panelColour = uICanvas.transform.Find("PanelHangar").Find("PanelInventory").Find("PanelColor").gameObject;
        GameObject weaponPanelActive = uICanvas.transform.Find("PanelHangar").Find("PanelInventory").Find("PanelWeaponActive").gameObject;
        GameObject colourPanelActive = uICanvas.transform.Find("PanelHangar").Find("PanelInventory").Find("PanelColourActive").gameObject;

        panelWeapon.SetActive(true);
        panelColour.SetActive(false);
        weaponPanelActive.SetActive(true);
        colourPanelActive.SetActive(false);
    }

    public void OnClickInventoryColor()
    {
        GameObject buttonColor = uICanvas.transform.Find("PanelHangar").Find("PanelInventory").Find("PanelTab").Find("Color").gameObject;
        GameObject buttonWeapon = uICanvas.transform.Find("PanelHangar").Find("PanelInventory").Find("PanelTab").Find("Weapon").gameObject;
        GameObject panelWeapon = uICanvas.transform.Find("PanelHangar").Find("PanelInventory").Find("PanelWeapon").gameObject;
        GameObject panelColour = uICanvas.transform.Find("PanelHangar").Find("PanelInventory").Find("PanelColor").gameObject;
        GameObject weaponPanelActive = uICanvas.transform.Find("PanelHangar").Find("PanelInventory").Find("PanelWeaponActive").gameObject;
        GameObject colourPanelActive = uICanvas.transform.Find("PanelHangar").Find("PanelInventory").Find("PanelColourActive").gameObject;

        panelWeapon.SetActive(false);
        panelColour.SetActive(true);
        weaponPanelActive.SetActive(false);
        colourPanelActive.SetActive(true);
    }

    public void OnClickPlayButton()
    {
        if (chanceObject.chanceNumber == "0")
        {
            Debug.Log("ChanceZero");
            StartCoroutine(ChanceEmpty());
        }
        else
        {
            Debug.Log("ChanceReady");
            StartCoroutine(LoadSceneAsynchronously("Gameplay"));
        }
    }

    public IEnumerator ChanceEmpty()
    {
        chanceKosongPanel.SetActive(true);
        yield return new WaitForSeconds(3f);
        chanceKosongPanel.SetActive(false);
    }
    public void OnClickPlayerShip()
    {
        uICanvas.SetActive(false);
        
        uICanvas.transform.Find("PanelMainMenu").gameObject.SetActive(false);


        player.GetComponent<Animator>().SetTrigger("GoingToHangar");
        player.GetComponent<Animator>().SetBool("IsInHangar", true);


        hangarBackground.GetComponent<Animator>().SetTrigger("IsArriving");
        StartCoroutine(waitForPlayerShipLanding());
    }

    IEnumerator waitForPlayerShipLanding()
    {
        Animator animator = hangarBackground.GetComponent<Animator>();

        yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length);

        player.transform.Find("Boosters").gameObject.SetActive(false);
        uICanvas.transform.Find("PanelHangar").gameObject.SetActive(true);
        HangarSound.Play();
        uICanvas.SetActive(true);
        BuddyPlacementCanvas.SetActive(true);
        
        effects.SetActive(false);
    }

    public void GoBackToMainMenu()
    {
        player.GetComponent<Animator>().SetBool("IsInHangar", false);
        hangarBackground.GetComponent<Animator>().SetTrigger("IsDeparting");
        hangarBackground.GetComponent<Animator>().ResetTrigger("IsArriving");

         StartCoroutine(waitForPlayerShipDepart());
        arCam.enabled = false;
        Vuforia.VuforiaBehaviour.Instance.enabled = false;
        uICanvas.transform.Find("PanelMainMenu").gameObject.SetActive(true);
        uICanvas.transform.Find("PanelHangar").gameObject.SetActive(false);
        BuddyPlacementCanvas.SetActive(false);
    }

    IEnumerator waitForPlayerShipDepart()
    {
        Animator animator = hangarBackground.GetComponent<Animator>();

        yield return new WaitForSeconds(animator.GetCurrentAnimatorStateInfo(0).length);

        player.transform.Find("Boosters").gameObject.SetActive(true);
        hangarBackground.GetComponent<Animator>().ResetTrigger("IsDeparting");
        effects.SetActive(true);
    }

    public void ReloadMainMenuScen()
    {
        StartCoroutine(LoadSceneAsynchronously("MainMenu"));
    }

    public void OnClickScanCardButton()
    {
        Vuforia.VuforiaBehaviour.Instance.enabled = true;
        arCam.enabled = true;
        uICanvas.SetActive(false);
        BuddyPlacementCanvas.SetActive(false);
        //aRCamera.SetActive(true);
        aRScanCard.SetActive(true);

        for (int i = 0; i < CardTarget.Length; i++)
        {
            CardTarget[i].SetActive(true);
        }

        aRCamera.transform.Find("SpaceCandyTarget").gameObject.SetActive(false);
        for (short i = 0; i < aRCamera.transform.childCount; i++)
        {
            GameObject child = aRCamera.transform.GetChild(i).gameObject;
            if (child.CompareTag(scanCardTag))
            {
                child.transform.GetChild(0).gameObject.SetActive(true);
            }
        }
    }

    public void OnClickARScanCardBackButton()
    {
        border1.SetActive(true);
        borderlogo1.SetActive(true);
        border2.SetActive(true);
        borderlogo2.SetActive(true);
        arText1.SetActive(true);
        arText2.SetActive(true);
        arCam.enabled = false;
        Vuforia.VuforiaBehaviour.Instance.enabled = false;
        aRScanCard.SetActive(false);
        uICanvas.SetActive(true);
    }

    public void OnClickARButton()
    {
        //aRCamera.SetActive(true);
        Vuforia.VuforiaBehaviour.Instance.enabled = true;
        arCam.enabled = true;
        uICanvas.SetActive(false);
        aRSpaceCandyPopUp.SetActive(true);

        for (int i = 0; i < CardTarget.Length; i++)
        {
            CardTarget[i].SetActive(false);
        }

        aRCamera.transform.Find("SpaceCandyTarget").gameObject.SetActive(true);
        for (short i = 0; i < aRCamera.transform.childCount; i++)
        {
            GameObject child = aRCamera.transform.GetChild(i).gameObject;
            if (child.CompareTag(scanCardTag))
            {
                child.transform.GetChild(0).gameObject.SetActive(false);
            }
        }

        clonedPlayer = Instantiate(player.transform.Find("SpaceCandy").gameObject, aRCamera.transform.Find("SpaceCandyTarget").position, aRCamera.transform.Find("SpaceCandyTarget").rotation, aRCamera.transform.Find("SpaceCandyTarget"));
    }

    public void OnClickARBackButton()
    {
        border1.SetActive(true);
        borderlogo1.SetActive(true);
        border2.SetActive(true);
        borderlogo2.SetActive(true);
        arText1.SetActive(true);
        arText2.SetActive(true);
        Vuforia.VuforiaBehaviour.Instance.enabled = false;
        arCam.enabled = false;
        uICanvas.SetActive(true);
        //aRCamera.SetActive(false);
        aRSpaceCandyPopUp.SetActive(false);

        Destroy(clonedPlayer);
    }

    public void OnClickCardDetailBackButton()
    {
        uICanvas.transform.Find("PanelHangar").Find("PanelCardDetail").gameObject.SetActive(false);
    }

    public void OnClickCard(Sprite cardImage, string cardTitle, float powerLevel, float attackSpeedLevel)
    {
        GameObject panelCardDetail = uICanvas.transform.Find("PanelHangar").Find("PanelCardDetail").gameObject;
        panelCardDetail.SetActive(true);
        panelCardDetail.transform.Find("Image").GetComponent<Image>().sprite = cardImage;
        panelCardDetail.transform.Find("Image").GetComponent<Image>().SetNativeSize();
    }

    public void settingon()
    {
        settingClose.SetActive(true);
        LeanTween.scale(settingButton, new Vector3(0f, 0f, 0f), 0.1f).setEase(LeanTweenType.easeOutBack).setDelay(0.1f);
        LeanTween.scale(settingPanel, new Vector3(0.807285f, 0.807285f, 0.807285f), 0.1f).setEase(LeanTweenType.easeOutBack).setDelay(0.1f);
    }
    public void settingoff()
    {
        settingClose.SetActive(false);
        LeanTween.scale(settingButton, new Vector3(5.716753f, 5.716753f, 5.716753f), 0.1f).setEase(LeanTweenType.easeOutBack).setDelay(0.1f);
        LeanTween.scale(settingPanel, new Vector3(0f, 0f, 0f), 0.1f).setEase(LeanTweenType.easeOutBack).setDelay(0.1f);
    }
}
