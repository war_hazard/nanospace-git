﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class PlayerMaterials : MonoBehaviour
{
    public MeshRenderer LeftWingShipgameplay;
    public MeshRenderer RightWingShipgameplay;
    public MeshRenderer mainbodyShipgameplay;
    public MeshRenderer bridgeShipgameplay;
    public MeshRenderer mochiLShipgameplay;
    public MeshRenderer mochiRShipgameplay;
    public MeshRenderer thruster1Shipgameplay;
    public MeshRenderer thruster2Shipgameplay;
    public MeshRenderer thruster3Shipgameplay;

    public static Material Lwing;
    public static Material Rwing;
    public static Material MainBody;
    public static Material bridge;
    public static Material mochiL;
    public static Material mochiR;
    public static Material thruster1;
    public static Material thruster2;
    public static Material thruster3;
    //public static HangarPlayerMaterial hangarPlayerMat;

    // Start is called before the first frame update
    void Start()
    {

        // material dari gameplay ngambil dari prefab kapal hangar
        DontDestroyOnLoad(gameObject);
        LeftWingShipgameplay.material = HangarPlayerMaterial.Lwing;
        RightWingShipgameplay.material = HangarPlayerMaterial.Rwing;
        mainbodyShipgameplay.material = HangarPlayerMaterial.MainBody;
        bridgeShipgameplay.material = HangarPlayerMaterial.bridge;
        mochiLShipgameplay.material = HangarPlayerMaterial.mochiL;
        mochiRShipgameplay.material = HangarPlayerMaterial.mochiR;
        thruster1Shipgameplay.material = HangarPlayerMaterial.thruster1;
        thruster2Shipgameplay.material = HangarPlayerMaterial.thruster2;
        thruster3Shipgameplay.material = HangarPlayerMaterial.thruster3;
    }

    // Update is called once per frame
    void Update()
    {
        //hangarPlayerMat = gameObject.GetComponent<HangarPlayerMaterial>();


        Scene currentScene = SceneManager.GetActiveScene();
        // Retrieve the name of this scene.
        string sceneName = currentScene.name;

        if (sceneName == "MainMenu")
        {
            gameObject.SetActive(true);
            StartCoroutine(countDowntoDestroy());

        }

    }

    public IEnumerator countDowntoDestroy()
    {
        //gameObject.SetActive(false);
        yield return new WaitForSeconds(0.01F);
        Destroy(gameObject);
    }
}
