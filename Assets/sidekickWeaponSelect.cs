﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sidekickWeaponSelect : MonoBehaviour
{
    public bool Sika;
    public bool Bobo;
    public bool Jellions;
    public string sidekickPlacementTagLeft = "sidekickPlacementLeft";
    public string sidekickPlacementTagRight = "sidekickPlacementRight";
    SikaController sidekickcontroller;
    public void Start()
    {
        

        //UIController.BuddyDragged1 = true;
        //UIController.BuddyDragged2 = true;


    }


    public void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag(sidekickPlacementTagLeft))
        {
            sidekickcontroller = GetComponent<SikaController>();
            sidekickcontroller.sikaShipOffset = new Vector3(-1.051f, 0.2f, -0.335f);
            //UIController.BuddyDragged2 = true;
            
            if (Sika)
            {
                GameController.sideKickSikaLeft = true;
                GameController.sideKickBoBoLeft = false;
                GameController.sideKickJellionsLeft = false;
                GameController.sideKickSikaRight = false;
                GameController.sideKickBoBoRight = false;
                GameController.sideKickJellionsRight = false;
            }
            if (Bobo)
            {
                GameController.sideKickSikaLeft = false;
                GameController.sideKickBoBoLeft = true;
                GameController.sideKickJellionsLeft = false;
                GameController.sideKickSikaRight = false;
                GameController.sideKickBoBoRight = false;
                GameController.sideKickJellionsRight = false;
            }
            if (Jellions)
            {
                GameController.sideKickSikaLeft = false;
                GameController.sideKickBoBoLeft = false;
                GameController.sideKickJellionsLeft = true;
                GameController.sideKickSikaRight = false;
                GameController.sideKickBoBoRight = false;
                GameController.sideKickJellionsRight = false;
            }

        }

        if (collision.gameObject.CompareTag(sidekickPlacementTagRight))
        {
            sidekickcontroller = GetComponent<SikaController>();
            sidekickcontroller.sikaShipOffset = new Vector3(1.051f, 0.2f, -0.335f);
            //UIController.BuddyDragged1 = true;
            
            if (Sika)
            {
                GameController.sideKickSikaLeft = false;
                GameController.sideKickBoBoLeft = false;
                GameController.sideKickJellionsLeft = false;
                GameController.sideKickSikaRight = true;
                GameController.sideKickBoBoRight = false;
                GameController.sideKickJellionsRight = false;
            }
            if (Bobo)
            {
                GameController.sideKickSikaLeft = false;
                GameController.sideKickBoBoLeft = false;
                GameController.sideKickJellionsLeft = false;
                GameController.sideKickSikaRight = false;
                GameController.sideKickBoBoRight = true;
                GameController.sideKickJellionsRight = false;
            }
            if (Jellions)
            {
                GameController.sideKickSikaLeft = false;
                GameController.sideKickBoBoLeft = false;
                GameController.sideKickJellionsLeft = false;
                GameController.sideKickSikaRight = false;
                GameController.sideKickBoBoRight = false;
                GameController.sideKickJellionsRight = true;
            }

        }

        if (collision.gameObject.CompareTag("PlayerShip"))
        {
            return;
        }
    }
    void OnApplicationQuit()
    {
        //UIController.BuddyDragged1 = false;
        //UIController.BuddyDragged2 = false;
        GameController.sideKickSikaRight = false;
        GameController.sideKickBoBoRight = false;
        GameController.sideKickJellionsRight = false;
        GameController.sideKickSikaLeft = false;
        GameController.sideKickBoBoLeft = false;
        GameController.sideKickJellionsLeft = false;
    }



}
