﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardManager : MonoBehaviour
{

    public int MarkerID;
    public GameObject Shooter;
    public GameObject Bomber;
    public GameObject Bouncher;
    public GameObject Shock;
    public GameObject Mutate;
    public GameObject Bubble;
    public GameObject Bulky;
    public GameObject Kard;
    public GameObject Bobo;
    public GameObject Jellions;
    public GameObject SiNano;
    public GameObject TrioSika;
    public GameObject Barricade;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ScanKartu()
    {
        if (MarkerID == 0)
        {
            PlayerPrefs.SetInt("ShooterCardScanned", 1);
            Shooter.SetActive(PlayerPrefs.GetInt("ShooterCardScanned", 0) == 0 ? true : false);

        }

        if (MarkerID == 2)
        {
            PlayerPrefs.SetInt("BomberCardScanned", 1);
            Bomber.SetActive(PlayerPrefs.GetInt("BomberCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerID == 4)
        {
            PlayerPrefs.SetInt("BouncherCardScanned", 1);
            Bouncher.SetActive(PlayerPrefs.GetInt("BouncherCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerID == 6)
        {
            PlayerPrefs.SetInt("ShockCardScanned", 1);
            Shock.SetActive(PlayerPrefs.GetInt("ShockCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerID == 7)
        {
            PlayerPrefs.SetInt("MutateCardScanned", 1);
            Mutate.SetActive(PlayerPrefs.GetInt("MutateCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerID == 8)
        {
            PlayerPrefs.SetInt("BubbleCardScanned", 1);
            Bubble.SetActive(PlayerPrefs.GetInt("BubbleCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerID == 9)
        {
            PlayerPrefs.SetInt("BulkyCardScanned", 1);
            Bulky.SetActive(PlayerPrefs.GetInt("BulkyCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerID == 10)
        {
            PlayerPrefs.SetInt("KARDCardScanned", 1);
            Kard.SetActive(PlayerPrefs.GetInt("KARDCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerID == 11)
        {
            PlayerPrefs.SetInt("BoBoCardScanned", 1);
            Bobo.SetActive(PlayerPrefs.GetInt("BoBoCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerID == 12)
        {
            PlayerPrefs.SetInt("JellionsCardScanned", 1);
            Jellions.SetActive(PlayerPrefs.GetInt("JellionsCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerID == 13)
        {
            PlayerPrefs.SetInt("SiNanoCardScanned", 1);
            SiNano.SetActive(PlayerPrefs.GetInt("SiNanoCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerID == 14)
        {
            PlayerPrefs.SetInt("TrioSikaCardScanned", 1);
            TrioSika.SetActive(PlayerPrefs.GetInt("TrioSikaCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerID == 15)
        {
            PlayerPrefs.SetInt("BarricadeCardScanned", 1);
            Barricade.SetActive(PlayerPrefs.GetInt("BarricadeCardScanned", 0) == 0 ? true : false);
        }
    }
}
