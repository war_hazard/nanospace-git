﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorCardObjectController : MonoBehaviour
{
    public Material mat;
    public bool hitMesh = false;
    public GameObject mainBody;
    public GameObject bridge;
    public GameObject WingL;
    public GameObject WingR;
    public GameObject MochiL;
    public GameObject MochiR;
    public GameObject truster1;
    public GameObject truster2;
    public GameObject truster3;

    MeshRenderer mainBodymesh;
    MeshRenderer bridgeMesh;
    MeshRenderer wingLmesh;
    MeshRenderer wingRmesh;
    MeshRenderer MochiLmesh;
    MeshRenderer MochiRmesh;
    MeshRenderer truster1Mesh;
    MeshRenderer truster2Mesh;
    MeshRenderer truster3Mesh;
    //private const string ObjectTagName = "WeaponPlacement";
    //public BoxCollider bc;
    // Start is called before the first frame update
    void Start()
    {
        mainBody = GameObject.Find("spaceCandy_body_hi01 COLOR");
        bridge = GameObject.Find("spaceCandy_bridge_hi01");
        WingL = GameObject.Find("spaceCandy_L_wing_hi01 COLOR");
        WingR = GameObject.Find("spaceCandy_R_wing_hi01 COLOR");
        MochiL = GameObject.Find("spaceCandy_L_mochi01");
        MochiR = GameObject.Find("spaceCandy_R_mochi01");
        truster1 = GameObject.Find("spaceCandy_L_thrusters01");
        truster2 = GameObject.Find("spaceCandy_thrusters01");
        truster3 = GameObject.Find("spaceCandy_R_thrusters01");

        mainBodymesh = mainBody.GetComponent<MeshRenderer>();
        bridgeMesh = bridge.GetComponent<MeshRenderer>();
        wingLmesh = WingL.GetComponent<MeshRenderer>();
        wingRmesh = WingR.GetComponent<MeshRenderer>();
        MochiLmesh = MochiL.GetComponent<MeshRenderer>();
        MochiRmesh = MochiR.GetComponent<MeshRenderer>();
        truster1Mesh = truster1.GetComponent<MeshRenderer>();
        truster2Mesh = truster2.GetComponent<MeshRenderer>();
        truster3Mesh = truster3.GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnMouseDown()
    {
        Destroy(gameObject);              
    }

    private void OnTriggerEnter(Collider other)
    {

        //Debug.Log("Trigger Enter something");
        if (other.CompareTag("wings"))
        {
            wingLmesh.material = mat;
            wingRmesh.material = mat;
            hitMesh = true;
        }

        if (other.CompareTag("bodymain"))
        {
            MochiLmesh.material = mat;
            MochiRmesh.material = mat;
            bridgeMesh.material = mat;
            hitMesh = true;
        }

        if (other.CompareTag("bodybase"))
        {
            mainBodymesh.material = mat;            
            hitMesh = true;
        }

        if (other.CompareTag("truster"))
        {
            truster1Mesh.material = mat;
            truster2Mesh.material = mat;
            truster3Mesh.material = mat;
            hitMesh = true;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        //Debug.Log("Trigger Exit something");
        hitMesh = false;
    }
}
