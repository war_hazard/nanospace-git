﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerDaysRemaining : MonoBehaviour
{
    public float timer;
    public Text[] DaysRemaining;
    int index;
    //bool FiveDaysRemaining = false;
    //bool FourDaysRemaining = false;
    //bool ThreeDaysRemaining = false;
    //bool TwodaysRemaining = false;
    //bool OneDaysRemaining = false;

    void Start()
    {
        timer = 7200;

        timer -= TimeMaster.instance.CheckDate();
    }

    void Update()
    {
        timer -= Time.deltaTime;

        if (timer == 7200) // 5 Days Remaining
        {
            //OneDaysRemaining = false;
            //FiveDaysRemaining = true;
            DaysRemaining[index].text = " 5   Days";
        }

        if (timer == 5760) // 4 Days Remaining
        {
            //FiveDaysRemaining = false;
            //FourDaysRemaining = true;
            DaysRemaining[index].text = " 4   Days";
        }

        if (timer == 4320) // 3 Days Remaining
        {
            //FourDaysRemaining = false;
            //ThreeDaysRemaining = true;
            DaysRemaining[index].text = " 3   Days";
        }

        if (timer == 2880) // 2 Days Remaining
        {
            //ThreeDaysRemaining = false;
            //TwodaysRemaining = true;
            DaysRemaining[index].text = " 2   Days";
        }

        if (timer == 1440) // 1 Days Remaining
        {
            //TwodaysRemaining = false;
            //OneDaysRemaining = true;
            DaysRemaining[index].text = " 1   Days";
        }




    }

    public void ResetClock() // to reset and start timer
    {
        TimeMaster.instance.SaveData();
        timer = 7200;
        timer -= TimeMaster.instance.CheckDate();
    }
}
