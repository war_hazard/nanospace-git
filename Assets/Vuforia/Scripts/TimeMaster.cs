﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TimeMaster : MonoBehaviour
{
    DateTime currentDate;
    DateTime oldDate;

    public string saveLocation;
    public static TimeMaster instance;
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;

        saveLocation = "lastSavedDate1";
    }

    public float CheckDate()
    {
        currentDate = System.DateTime.Now;

        string tempString = PlayerPrefs.GetString(saveLocation, "1");

        long tempLong = Convert.ToInt64(tempString);

        DateTime oldDate = DateTime.FromBinary(tempLong);
        print("Old Date : " + oldDate);

        TimeSpan differnce = currentDate.Subtract(oldDate);
        print("Difference: " + differnce);

        return (float)differnce.TotalSeconds;

    }

    public void SaveData() 
    {
        PlayerPrefs.SetString(saveLocation, System.DateTime.Now.ToBinary().ToString());

        print("Saving this date to playerprefs: " + System.DateTime.Now);
    }
}
