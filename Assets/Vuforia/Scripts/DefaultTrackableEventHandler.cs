/*==============================================================================
Copyright (c) 2019 PTC Inc. All Rights Reserved.

Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Protected under copyright and other laws.
==============================================================================*/

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Vuforia;


/// <summary>
/// A custom handler that implements the ITrackableEventHandler interface.
///
/// Changes made to this file could be overwritten when upgrading the Vuforia version.
/// When implementing custom event handler behavior, consider inheriting from this class instead.
/// </summary>
public class DefaultTrackableEventHandler : MonoBehaviour, ITrackableEventHandler
{
    public GameObject border1;
    public GameObject borderlogo1;
    public GameObject border2;
    public GameObject borderlogo2;
    public GameObject arText1;
    public GameObject arText2;
    [HideInInspector]
    private Vector3 startPosition;
    public int pernahTrack = 0;
    public int MarkerIndex;
    public UnityEngine.UI.Image CurrentImg;
    public UnityEngine.UI.Image ShinnyStatusImg;
    public Sprite ShinnyOn;
    public Sprite ShinnyOff;
    
    public GameObject NotScannedShooter;
    public GameObject NotScannedBomber;
    public GameObject NotScannedBouncher;
    public GameObject NotScannedShock;
    public GameObject NotScannedMutate;
    public GameObject NotScannedBubble;
    public GameObject NotScannedBulky;
    public GameObject NotScannedKard;
    public GameObject NotScannedBobo;
    public GameObject NotScannedJellions;
    public GameObject NotScannedSiNano;
    public GameObject NotScannedTrioSika;
    public GameObject NotScannedBarricade;

    public static bool isScanned = false;
    bool bolehScan = false;

    //public CardScanManager crdScMngr;
    
    public bool isSpecialCard;
    public bool isShinny;

    //public CardController crd;
    //public GameObject blocker;
    
    //Timer 
    //public Text[] DisplayDaysRemaining;
    //int index;
    
    //public TimerDaysRemaining IndicatorTimerDaysRemaining;
    #region PROTECTED_MEMBER_VARIABLES

    protected TrackableBehaviour mTrackableBehaviour;
    protected TrackableBehaviour.Status m_PreviousStatus;
    protected TrackableBehaviour.Status m_NewStatus;

    #endregion // PROTECTED_MEMBER_VARIABLES

    #region UNITY_MONOBEHAVIOUR_METHODS

    protected virtual void Start()
    {
        
        startPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);

        //if (IndicatorTimerDaysRemaining == null)     
        //    return;

        //if (DisplayDaysRemaining == null)
        //    return;

        //DisplayDaysRemaining[index].text = IndicatorTimerDaysRemaining.timer.ToString();

        pernahTrack = PlayerPrefs.GetInt("pernahTrack" + MarkerIndex.ToString(), 0); // apakah marker udah pernah ditrack apa belum

        mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        if (mTrackableBehaviour)
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
    }

    protected virtual void OnDestroy()
    {
        if (mTrackableBehaviour)
            mTrackableBehaviour.UnregisterTrackableEventHandler(this);
    }

    #endregion // UNITY_MONOBEHAVIOUR_METHODS

    #region PUBLIC_METHODS

    /// <summary>
    ///     Implementation of the ITrackableEventHandler function called when the
    ///     tracking state changes.
    /// </summary>
    public void OnTrackableStateChanged(
        TrackableBehaviour.Status previousStatus,
        TrackableBehaviour.Status newStatus)
    {
        m_PreviousStatus = previousStatus;
        m_NewStatus = newStatus;
        
        Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + 
                  " " + mTrackableBehaviour.CurrentStatus +
                  " -- " + mTrackableBehaviour.CurrentStatusInfo);

        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
        {
            OnTrackingFound();
        }
        else if (previousStatus == TrackableBehaviour.Status.TRACKED &&
                 newStatus == TrackableBehaviour.Status.NO_POSE)
        {
            OnTrackingLost();
        }
        else
        {
            // For combo of previousStatus=UNKNOWN + newStatus=UNKNOWN|NOT_FOUND
            // Vuforia is starting, but tracking has not been lost or found yet
            // Call OnTrackingLost() to hide the augmentations
            OnTrackingLost();
        }
    }

    #endregion // PUBLIC_METHODS

    #region PROTECTED_METHODS

    protected virtual void OnTrackingFound()
    {
        Debug.Log($"{transform.GetChild(0).gameObject.name} Marker Found!");

        if (mTrackableBehaviour)
        {
            var rendererComponents = mTrackableBehaviour.GetComponentsInChildren<Renderer>(true);
            var colliderComponents = mTrackableBehaviour.GetComponentsInChildren<Collider>(true);
            var canvasComponents = mTrackableBehaviour.GetComponentsInChildren<Canvas>(true);

            // Enable rendering:
            foreach (var component in rendererComponents)
                component.enabled = true;

            // Enable colliders:
            foreach (var component in colliderComponents)
                component.enabled = true;

            // Enable canvas':
            foreach (var component in canvasComponents)
                component.enabled = true;
        }
        border1.SetActive(false);
        borderlogo1.SetActive(false);
        border2.SetActive(false);
        borderlogo2.SetActive(false);
        arText1.SetActive(false);
        arText2.SetActive(false);

        

        //crdScMngr.markerID = MarkerIndex;

        if (pernahTrack == 0)
        {
            PlayerPrefs.SetInt("pernahTrackFoto" + MarkerIndex.ToString(), 1);
            pernahTrack = PlayerPrefs.GetInt("pernahTrackFoto" + MarkerIndex.ToString(), 0);
            bolehScan = true;

            if(bolehScan)
            {
                if (PlayerPrefs.GetInt("pernahTrackFoto" + MarkerIndex.ToString()) == 1) //setiap 1 kali scan akan mendapatkan skor,setelah lebih dari 1 scan maka sudah tidak bisa menambah skor
                {
                    //isScanned = true;
                    CheckMarker();
                }
            }

        }
    }


    protected virtual void OnTrackingLost()
    {
        if (mTrackableBehaviour)
        {
            var rendererComponents = mTrackableBehaviour.GetComponentsInChildren<Renderer>(true);
            var colliderComponents = mTrackableBehaviour.GetComponentsInChildren<Collider>(true);
            var canvasComponents = mTrackableBehaviour.GetComponentsInChildren<Canvas>(true);

            transform.position = startPosition;

            // Disable rendering:
            foreach (var component in rendererComponents)
                component.enabled = false;

            // Disable colliders:
            foreach (var component in colliderComponents)
                component.enabled = false;

            // Disable canvas':
            foreach (var component in canvasComponents)
                component.enabled = false;
        }

        border1.SetActive(true);
        borderlogo1.SetActive(true);
        border2.SetActive(true);
        borderlogo2.SetActive(true);
        arText1.SetActive(true);
        arText2.SetActive(true);
    }

    public void CheckMarker()
    {


        if (MarkerIndex == 0)
        {
            PlayerPrefs.SetInt("ShooterCardScanned", 1);
            NotScannedShooter.SetActive(PlayerPrefs.GetInt("ShooterCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerIndex == 2)
        {
            PlayerPrefs.SetInt("BomberCardScanned", 1);
            NotScannedBomber.SetActive(PlayerPrefs.GetInt("BomberCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerIndex == 4)
        {
            PlayerPrefs.SetInt("BouncherCardScanned", 1);
            NotScannedBouncher.SetActive(PlayerPrefs.GetInt("BouncherCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerIndex == 6)
        {
            PlayerPrefs.SetInt("ShockCardScanned", 1);
            NotScannedShock.SetActive(PlayerPrefs.GetInt("ShockCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerIndex == 7)
        {
            PlayerPrefs.SetInt("MutateCardScanned", 1);
            NotScannedMutate.SetActive(PlayerPrefs.GetInt("MutateCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerIndex == 8)
        {
            PlayerPrefs.SetInt("BubbleCardScanned", 1);
            NotScannedBubble.SetActive(PlayerPrefs.GetInt("BubbleCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerIndex == 9)
        {
            PlayerPrefs.SetInt("BulkyCardScanned", 1);
            NotScannedBulky.SetActive(PlayerPrefs.GetInt("BulkyCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerIndex == 10)
        {
            PlayerPrefs.SetInt("KARDCardScanned", 1);
            NotScannedKard.SetActive(PlayerPrefs.GetInt("KARDCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerIndex == 11)
        {
            PlayerPrefs.SetInt("BoBoCardScanned", 1);
            NotScannedBobo.SetActive(PlayerPrefs.GetInt("BoBoCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerIndex == 12)
        {
            PlayerPrefs.SetInt("JellionsCardScanned", 1);
            NotScannedJellions.SetActive(PlayerPrefs.GetInt("JellionsCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerIndex == 13)
        {
            PlayerPrefs.SetInt("SiNanoCardScanned", 1);
            NotScannedSiNano.SetActive(PlayerPrefs.GetInt("SiNanoCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerIndex == 14)
        {
            PlayerPrefs.SetInt("TrioSikaCardScanned", 1);
            NotScannedTrioSika.SetActive(PlayerPrefs.GetInt("TrioSikaCardScanned", 0) == 0 ? true : false);
        }

        if (MarkerIndex == 15)
        {
            PlayerPrefs.SetInt("BarricadeCardScanned", 1);
            NotScannedBarricade.SetActive(PlayerPrefs.GetInt("BarricadeCardScanned", 0) == 0 ? true : false);
        }


        //if (isShinny == false)
        //{
        //    ShinnyStatusImg = null;

        //    return;            
        //}

        //if (isSpecialCard)
        //{
        //    // kondisi timer berlaku untuk 5 hari kedepan 
        //    // IndicatorTimerDaysRemaining.ResetClock();

        //    if (isShinny)
        //    {                
        //        ShinnyStatusImg.sprite = ShinnyOn;
        //    }
        //    else
        //    {
        //        ShinnyStatusImg.sprite = ShinnyOff;
        //    }

        //}

    }

    void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("ShooterCardScanned", 0);
        PlayerPrefs.SetInt("BomberCardScanned", 0);
        PlayerPrefs.SetInt("BouncherCardScanned", 0);
        PlayerPrefs.SetInt("ShockCardScanned", 0);
        PlayerPrefs.SetInt("MutateCardScanned", 0);
        PlayerPrefs.SetInt("BubbleCardScanned", 0);
        PlayerPrefs.SetInt("BulkyCardScanned", 0);
        PlayerPrefs.SetInt("KARDCardScanned", 0);
        PlayerPrefs.SetInt("BoBoCardScanned", 0);
        PlayerPrefs.SetInt("JellionsCardScanned", 0);
        PlayerPrefs.SetInt("SiNanoCardScanned", 0);
        PlayerPrefs.SetInt("TrioSikaCardScanned", 0);
        PlayerPrefs.SetInt("BarricadeCardScanned", 0);
    }


    #endregion // PROTECTED_METHODS
}
