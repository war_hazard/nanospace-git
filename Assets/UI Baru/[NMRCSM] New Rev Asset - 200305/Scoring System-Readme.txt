Detail Score:

Enemies
1. Kill Hunter Krace >> 10.000
2. kILL Dark Jetka Ver.1 >> 15.000
3. Kill Gigan Krace >> 20.000
4. Kill Dark Jetka Ver.2 >> 25.000
5. Kill Dome Jetka >> 40.000

Items
1. Calcium Orb >> 5.000
2. Ex Skill >> 7.000
3. Nano-Nano Bombard >> 15.000

Boss
1. Boss Stage 1 >> 150.000
2. Boss Stage 2 >> 300.000
3. Boss Stage 3 >> 450.000
4. Boss Stage 4 >> 600.000
5. Boss Stage 5 >> 750.000

Multiplied Score
1. Collection unlock 2 >> x1,15%
2. Collection unlock 4 >> x1,3%
3. Collection unlock 6 >> x1,45%
4. Collection unlock 8 >> x1,6%
5. Collection unlock 10 >> x1,75%
6. Collection unlock 12 >> x1,9%
7. Collection uncock 14 >> x2,1%
8. Collection unlock 16 >> x2,25%