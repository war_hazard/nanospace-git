>>
Untuk bagian ini mohon diperhatikan di Buddy spot,
karena asset yang dipersiapkan ada dua dimana salah satu berdiri sendiri, dan satunya bersamaan seukuran artboard.
Penggunaan mengikuti mudahnya implementasi dari FE karena perlu di sesuaikan tupang tindaihnya dengan buddy spot putih.

PENTING!
ketika pesawat buddy ditaruh, maka buddy spot putih akan hilang, namun BG / Back buddy spot tetap fix di bawahnya.
Mungkin pemposisiannya dapat disamakan dan digerakkan seperti base BG hangar.


>>
Untuk bagian scan masing2 memiliki alternatif asset frame,
yang dapat disesuaikan dengan kebutuhan. Ingin menggunakan yang 1 asset langsung ditaruh maupun
pecahan asset yang fungsinya untuk mengikuti dimensi HP yang berbeda.