>>>
DETAIL STAGE enemies:
>
Stage1 (muncul boss 2x):
Minions_ didominasi Hunter Krace
Boss_ Doom warna ungu (default) (Senjata tipe 1)
>
Stage2 (muncul boss 2x):
Minions_ didominasi Hunter Krace dengan munculnya beberapa Dark Jetka Ver.1 (Jellions)
Boss_ Doom warna biru (Senjata tipe 1)
>
Stage3 (muncul boss 2x):
Minions_ didominasi Hunter Krace dan Dark Jetka Ver.1, sesekali muncul Gigan Krace
Boss_ Doom warna hijau (Senjata tipe 1 & 2)
>
Stage4 (muncul boss 2x):
Minions_ didominasi Hunter Krace dan Gigan Krace, sesekali muncul Dark Jetka Ver.1 dan Dark Jetka Ver.2
Boss_ Doom warna merah (Senjata tipe 1 & 2)
>
Stage5 (muncul boss unlimited):
Minions_ didominasi Hunter Kraced dan Gigan Krace, Dark Jetka Ver.1 dan Ver.2, sesekali muncul Dark Dome Jetka
Boss_ Doom warna Kuning (Senjata tipe 1, 2 & 3)
Perkembangan boss bisa dari kekuatan ataupun munculny minions dan intensitas kecepatan serangan.