﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using SimpleJSON;
using UnityEngine.Networking;

public class LoginManager : MonoBehaviour
{
    private string Ki = "R4Ha5Ia N4n0M1LkY2020";

    private string status, msg_system, token, msg, nama, email, pass;
    private string user_id, nohp, point, user_id_app, nama_app, email_app;

    public InputField id_input, pass_input;
    public InputField id_input_reg, pass_input_reg, email_input_reg, umur_input_reg;
    public Dropdown gender_input_reg, kota_input_reg;
    public GameObject regison, regisoff, loginon, loginoff;
    public Toggle tnc;

    public GameObject regis, reset_pass, login, error, errornameused, signed_in, registered, resetted;
    public Text error_txt;

    public string[] city_id, name, place;
    public Dropdown dropdown_region;
    private string tipe_kompetisi;
    private Dictionary<string, string> idPlace;
    List<string> list;

    private GameObject media;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(GetRegion());
        list = new List<string>();
        EncryptionKeys();

        if (EncryptedPlayerPrefs.HasKey("LoggedIn"))
        {
            if (EncryptedPlayerPrefs.GetString("LoggedIn") == "true")
            {
                StartCoroutine(signed());
            }
        }
    }
    public void open_error(string msg)
    {
        error.SetActive(true);
        error_txt.text = msg;
        StartCoroutine(test());
    }
    private IEnumerator test()
    {
        yield return new WaitForSeconds(2f);
        error.SetActive(false);
        id_input.text = "";
        pass_input.text = "";
    }

    public void open_error_nameused(string msg)
    {
        errornameused.SetActive(true);
        error_txt.text = msg;
        StartCoroutine(test1());
    }
    private IEnumerator test1()
    {
        yield return new WaitForSeconds(2f);
        errornameused.SetActive(false);
        id_input.text = "";
        pass_input.text = "";
    }

    private IEnumerator signed()
    {
        signed_in.SetActive(true);
        yield return new WaitForSeconds(1.0f);
        //SceneManager.LoadScene("MainMenu");
        SceneManager.LoadScene("Bridge");
    }
    private void EncryptionKeys()
    {
        EncryptedPlayerPrefs.keys = new string[5];
        EncryptedPlayerPrefs.keys[0] = "56Yyudra";
        EncryptedPlayerPrefs.keys[1] = "WI9DufYr";
        EncryptedPlayerPrefs.keys[2] = "idA5rAF2";
        EncryptedPlayerPrefs.keys[3] = "tHak3epr";
        EncryptedPlayerPrefs.keys[4] = "jat5eDTs";
    }

    // Update is called once per frame
    void Update()
    {
        if (id_input_reg.text == "" || pass_input_reg.text == "" || email_input_reg.text == "" || umur_input_reg.text == "" || gender_input_reg.options[gender_input_reg.value].text == "Gender" || kota_input_reg.options[kota_input_reg.value].text == "Kota")
        {
            regisoff.SetActive(true);
            regison.SetActive(false);
        }
        else
        {
            regison.SetActive(true);
            regisoff.SetActive(false);
        }

        if (id_input.text == "" || pass_input.text == "")
        {
            loginoff.SetActive(true);
            loginon.SetActive(false);
        }
        else
        {
            loginoff.SetActive(false);
            loginon.SetActive(true);
        }
    }
    public void default_login()
    {
        signed_in.SetActive(true);

        StartCoroutine(LoginRequest());
    }
    public void signup()
    {
        StartCoroutine(SignUpRequest());
    }
    private IEnumerator LoginRequest()
    {
        string url = RequestManager.HOST + "login";

        //email = "gilang@ar-innovation.com";
        //pass = "gilang123";

        WWWForm form = new WWWForm();
        form.AddField("nama", id_input.text);
        form.AddField("password", pass_input.text);
        WWW www = new WWW(url, form);

        StartCoroutine(WaitForRequestLogin(www));

        yield return null;
    }

    private IEnumerator WaitForRequestLogin(WWW www)
    {
        yield return www;

        // check for errors
        if (www.error == null)
        {
            Debug.Log("WWW Ok!: " + www.text);

            var N = JSONNode.Parse(www.text);

            status = N["status"];
            Debug.Log("status: " + status);

            if (status == "true")
            {
                //Debug.Log("kelogin");
                token = N["data"]["token"];

                EncryptedPlayerPrefs.SetString("token", token);
                EncryptedPlayerPrefs.SetString("login", "default");

                //ScoreManager.sc.token = token;
                //ScoreManager.sc.ki = Ki;
                StartCoroutine(ProfileGet());
            }
            else if (status == "false")
            {
                if (N["data"]["message"].Count == 0)
                {
                    msg = N["data"]["message"];
                }
                else msg = N["data"]["message"][0];

                signed_in.SetActive(false);
                open_error(msg);

                Debug.Log("msg: " + msg);
            }
        }
        else
        {
            signed_in.SetActive(false);
            Debug.Log("WWW Error: " + www.error);
        }
    }

    private IEnumerator ProfileGet()
    {
        string url = RequestManager.HOST + "profile";

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("authorization", token);
        headers.Add("key", Ki);
        WWW www = new WWW(url, null, headers);
        yield return www;
        Debug.Log(www.text);

        var N = JSONNode.Parse(www.text);

        status = N["status"];

        if (status == "true")
        {
            user_id_app = N["data"]["my_profile"]["user_id"];
            nama_app = N["data"]["my_profile"]["name"];
            nohp = N["data"]["my_profile"]["hp"];
            email_app = N["data"]["my_profile"]["email"];
            point = N["data"]["my_profile"]["point"];

            EncryptedPlayerPrefs.SetString("LoggedIn", "true");

            //SceneManager.LoadScene("MainMenu");
            SceneManager.LoadScene("Bridge");
        }
        else if (status == "false")
        {
            msg = N["message"][0];
            Debug.Log("msg: " + msg);

            signed_in.SetActive(false);
            open_error(msg);

            /*message_box.SetActive(true);
            message_img.GetComponent<Image>().sprite = gagal;
            message_text.text = msg;
            */

            //open_msg(msg);
        }
    }

    private IEnumerator SignUpRequest()
    {
        string url = RequestManager.HOST + "signup";

        WWWForm form = new WWWForm();
        form.AddField("nama", id_input_reg.text);
        form.AddField("password", pass_input_reg.text);
        form.AddField("email", email_input_reg.text);
        form.AddField("umur", umur_input_reg.text);
        if (gender_input_reg.options[gender_input_reg.value].text == "Laki-Laki")
        {
            form.AddField("gender", gender_input_reg.options[gender_input_reg.value].text = "L");
        }
        else
        {
            form.AddField("gender", gender_input_reg.options[gender_input_reg.value].text = "P");
        }
        form.AddField("gender", gender_input_reg.options[gender_input_reg.value].text);
        form.AddField("city_id", idPlace[kota_input_reg.options[kota_input_reg.value].text]);
        form.AddField("tnc", tnc.isOn ? "1" : "0");
        WWW www = new WWW(url, form);

        StartCoroutine(WaitForRequestSignUp(www));

        yield return null;
    }
    private IEnumerator WaitForRequestSignUp(WWW www)
    {
        yield return www;

        // check for errors
        if (www.error == null)
        {
            Debug.Log("WWW Ok!: " + www.text);

            var N = JSONNode.Parse(www.text);

            status = N["status"];
            msg_system = N["message_system"];
            Debug.Log("status: " + status);

            if (status == "true")
            {
                //Debug.Log("kelogin");
                //token = N["data"]["token"];
                //EncryptedPlayerPrefs.SetString("token", token);

                id_input_reg.text = "";
                pass_input_reg.text = "";
                email_input_reg.text = "";
                umur_input_reg.text = "";
                //gender_input_reg.options[gender_input_reg.value].text = "Gender";
                //kota_input_reg.options[kota_input_reg.value].text = "Kota";
                regis.SetActive(false);
                registered.SetActive(true);
                yield return new WaitForSeconds(3f);
                registered.SetActive(false);

                //ScoreManager.sc.token = token;
                //ScoreManager.sc.ki = Ki;
                //StartCoroutine(ProfileGet());
            }
            else if (status == "false")
            {
                if (N["data"]["message"].Count == 0)
                {
                    msg = N["data"]["message"];
                }
                else msg = N["data"]["message"][0];

                Debug.Log("msg: " + msg);

                if (msg_system == "name already used")
                {
                    open_error_nameused(msg);
                }
                else
                {
                    open_error(msg);
                }

                
                id_input_reg.text = "";
                pass_input_reg.text = "";
                email_input_reg.text = "";
                umur_input_reg.text = "";
                //gender_input_reg.options[gender_input_reg.value].text = "Gender";
                //kota_input_reg.options[kota_input_reg.value].text = "Kota";
            }
        }
        else
        {
            Debug.Log("WWW Error: " + www.error);
        }
    }

    //DROPDOWN

    private IEnumerator GetRegion()
    {
        string url = RequestManager.HOST + "city";
        WWW www = new WWW(url);
        yield return www;

        var N = JSONNode.Parse("www = " + www.text);

        int countregion = N["data"]["city"].Count;

        status = N["status"];
        //Debug.Log("region = " + N["data"]["region"][0]["region_id"]);

        if (status == "true")
        {
            //new array string data
            city_id = new string[countregion];
            name = new string[countregion];

            idPlace = new Dictionary<string, string>();

            for (int i = 0; i < countregion; i++)
            {
                //Debug.Log("i = " + i);
                city_id[i] = N["data"]["city"][i]["city_id"];
                name[i] = N["data"]["city"][i]["name"];


                list.Add(name[i]);
                idPlace.Add(name[i], city_id[i]);

            }

            dropdown_region.AddOptions(list);
            //dropdownregionachange();
            //Debug.Log("Haha");

        }
        else if (status == "false")
        {
            msg = N["message_system"][0];
            Debug.Log("msg: " + msg);
        }

        yield return null;
    }
}
