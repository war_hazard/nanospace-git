﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SplashScreen : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Starting Splash Screen");
        StartCoroutine(Count());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public IEnumerator Count()
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("StoryPanel");
    }
}
