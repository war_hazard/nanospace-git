﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using SimpleJSON;

public class LogoutManager : MonoBehaviour
{

    private string Ki = "R4Ha5Ia N4n0M1LkY2020";
    private string status, token, msg, nama, email, pass;
    // Start is called before the first frame update
    void Start()
    {
        EncryptionKeys();
        token = EncryptedPlayerPrefs.GetString("token");
    }
    private void EncryptionKeys()
    {
        EncryptedPlayerPrefs.keys = new string[5];
        EncryptedPlayerPrefs.keys[0] = "56Yyudra";
        EncryptedPlayerPrefs.keys[1] = "WI9DufYr";
        EncryptedPlayerPrefs.keys[2] = "idA5rAF2";
        EncryptedPlayerPrefs.keys[3] = "tHak3epr";
        EncryptedPlayerPrefs.keys[4] = "jat5eDTs";
    }
    public void logout()
    {
        if (EncryptedPlayerPrefs.HasKey("login"))
        {
            StartCoroutine(LogOutGet());
        }
    }
    private IEnumerator LogOutGet()
    {
        string url = RequestManager.HOST + "logout";

        Dictionary<string, string> headers = new Dictionary<string, string>();
        headers.Add("authorization", token);
        headers.Add("key", Ki);
        WWW www = new WWW(url, null, headers);
        yield return www;
        Debug.Log(www.text);

        var N = JSONNode.Parse(www.text);

        status = N["status"];

        if (status == "true")
        {
            yield return new WaitForSeconds(0.5f);
            EncryptedPlayerPrefs.DeleteKey("LoggedIn");
            EncryptedPlayerPrefs.DeleteKey("token");
            PlayerPrefs.DeleteAll();

            SceneManager.LoadScene("Login");
        }
        else if (status == "false")
        {
            msg = N["message"][0];
            Debug.Log("msg: " + msg);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
