﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HangarPlayerMaterial : MonoBehaviour
{
    // script kapal di main menu untuk di get oleh script PlayerMaterial agar warna yg sudah di apply di hangar dapat di apply di gameplay

    //public  GameObject GoLeftWingShipHangar;
    //public  GameObject GoRightWingShipHangar;
    //public  GameObject GomainbodyShipHangar;
    //public  GameObject GobridgeShipHangar;
    //public  GameObject GomochiLShipHangar;
    //public  GameObject GomochiRShipHangar;
    //public  GameObject Gothruster1ShipHangar;
    //public  GameObject Gothruster2ShipHangar;
    //public  GameObject Gothruster3ShipHangar;

    public  MeshRenderer LeftWingShipHangar;
    public  MeshRenderer RightWingShipHangar;
    public  MeshRenderer mainbodyShipHangar;
    public  MeshRenderer bridgeShipHangar;
    public  MeshRenderer mochiLShipHangar;
    public  MeshRenderer mochiRShipHangar;
    public  MeshRenderer thruster1ShipHangar;
    public  MeshRenderer thruster2ShipHangar;
    public  MeshRenderer thruster3ShipHangar;

    public static  Material Lwing;
    public static  Material Rwing;
    public static  Material MainBody;
    public static  Material bridge;
    public static  Material mochiL;
    public static  Material mochiR;
    public static  Material thruster1;
    public static  Material thruster2;
    public static  Material thruster3;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);

    }

    // Update is called once per frame
    void Update()
    {
        Lwing = LeftWingShipHangar.material;
        Rwing = RightWingShipHangar.material;
        MainBody = mainbodyShipHangar.material;
        bridge = bridgeShipHangar.material;
        mochiL = mochiLShipHangar.material;
        mochiR = mochiRShipHangar.material;
        thruster1 = thruster1ShipHangar.material;
        thruster2 = thruster2ShipHangar.material;
        thruster3 = thruster3ShipHangar.material;

        Scene currentScene = SceneManager.GetActiveScene();
        // Retrieve the name of this scene.
        string sceneName = currentScene.name;

        if (sceneName == "Gameplay")
        {
            StartCoroutine(countDowntoDestroyGamePlay());
        }

        if (sceneName == "MainMenu")
        {
            if (GameController.gemOver)
            {
                //toHangar();
                GameController.gemOver = false;
            }
        }

    }

    public IEnumerator countDowntoDestroyGamePlay()
    {
        yield return new WaitForSeconds(0.1f);
        Destroy(gameObject);
    }

    //public IEnumerator countDowntoDestroyMenu()
    //{

    //    yield return new WaitForSeconds(0.1f);
    //    Destroy(gameObject);
    //}

    public void toHangar()
    {
        LeftWingShipHangar.material = PlayerMaterials.Lwing;//PlayerMaterials.Lwing;
        RightWingShipHangar.material = PlayerMaterials.Rwing;
        mainbodyShipHangar.material = PlayerMaterials.MainBody;
        bridgeShipHangar.material = PlayerMaterials.bridge;
        mochiLShipHangar.material = PlayerMaterials.mochiL;
        mochiRShipHangar.material = PlayerMaterials.mochiR;
        thruster1ShipHangar.material = PlayerMaterials.thruster1;
        thruster2ShipHangar.material = PlayerMaterials.thruster2;
        thruster3ShipHangar.material = PlayerMaterials.thruster3;
        //GameController.gemOver = false;
    }

}
